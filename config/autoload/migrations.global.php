<?php

return [
    'doctrine' => [
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => __DIR__ . '/../../database/migrations/',
                'name' => 'Gdru Migrations',
                'namespace' => 'Migrations',
                'table' => 'doctrine_migrations',
            ],
        ],
    ],
];