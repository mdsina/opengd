<?php

namespace Auth\Controller;

use Auth\Enum\Permissions;
use Auth\Utils\RoleUtils;
use Core\Controller\JsonViewController;

class AuthController extends JsonViewController
{
    /**
     * @return \Zend\View\Model\JsonModel
     */
    public function getPermissionsListAction()
    {
        return $this->jsonData(Permissions::getList());
    }

    /**
     * @return \Zend\View\Model\JsonModel
     */
    public function getCurrentPermissionsAction()
    {
        $permission = RoleUtils::getRolesPermissions(
            $this->identity()->getRoles(),
            ['extract_type' => $this->params()->fromRoute('display')]
        );

        return $this->jsonData(['data' => $permission]);
    }

    public function isGrantedAction()
    {

    }
}