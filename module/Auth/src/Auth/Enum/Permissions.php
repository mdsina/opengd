<?php

namespace Auth\Enum;


use Core\Structure\AbstractEnum;

class Permissions extends AbstractEnum
{
    const ADMIN_VIEW = 'admin.view';
    const ADMIN_NAVIGATION_VIEW = 'admin.navigation.view';

    const ADMIN_FORUMS_ADD = 'admin.forums.add';
    const ADMIN_FORUMS_VIEW = 'admin.forums.view';
    const ADMIN_FORUMS_EDIT = 'admin.forums.edit';
    const ADMIN_FORUMS_REMOVE = 'admin.forums.remove';

    const ADMIN_USERS_ADD = 'admin.users.add';
    const ADMIN_USERS_BAN = 'admin.users.ban';
    const ADMIN_USERS_VIEW = 'admin.users.view';
    const ADMIN_USERS_EDIT = 'admin.users.edit';
    const ADMIN_USERS_REMOVE = 'admin.users.remove';
    const ADMIN_USERS_SEND_MESSAGE = 'admin.users.send_message';

    const FORUMS_VIEW = 'forums.view';
    const FORUMS_SUBSCRIBE = 'forums.subscribe';
    const FORUMS_ADD_TO_FAV = 'forums.add_to_favourite';

    // общие и пользовательские привилегии тем
    const TOPICS_VIEW = 'topics.view';
    const TOPICS_CREATE = 'topics.create';
    const TOPICS_OWN_EDIT = 'topics.own.edit';
    const TOPICS_SUBSCRIBE = 'topics.subscribe';
    const TOPICS_OWN_CLOSE = 'topics.own.close';
    const TOPICS_OWN_REMOVE = 'topics.own.remove';
    const TOPICS_OWN_MANAGE_IMPORTANCE = 'topics.own.manage_importance';

    // модераторские привилегии тем
    const TOPICS_EDIT = 'topics.edit';
    const TOPICS_CLOSE = 'topics.close';
    const TOPICS_REMOVE = 'topics.remove';
    const TOPICS_MANAGE_IMPORTANCE = 'topics.manage_importance';

    const MESSAGES_VIEW = 'messages.view';
    const MESSAGES_CREATE = 'messages.create';
    const MESSAGES_COMPLAIN = 'messages.complain';
    const MESSAGES_OWN_EDIT = 'messages.own.edit';
    const MESSAGES_OWN_REMOVE = 'messages.own.remove';
    const MESSAGES_ADD_TO_FAV = 'messages.add_to_favourite';

    const MESSAGES_EDIT = 'messages.edit';
    const MESSAGES_MOVE = 'messages.move';
    const MESSAGES_ADD_WARNING = 'messages.add_warning';

    const USERS_VIEW = 'users.info.view';
    const USERS_SEARCH = 'users.search';
    const USERS_PROFILE_VIEW = 'users.profile.view';
    const USERS_PROFILE_EDIT = 'users.profile.edit';
    const USERS_SEND_MESSAGE = 'users.send_message';
    const USERS_MANAGE_FRIENDS = 'users.manage_friends';
    const USERS_MANAGE_BLOCKED = 'users.manage_blocked';

}