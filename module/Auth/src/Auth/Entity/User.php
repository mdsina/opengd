<?php

namespace Auth\Entity;

use ZfcUser\Entity\User as ZfcUserEntity;
use ZfcRbac\Identity\IdentityInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ZfcUser\Entity\UserInterface;

/**
 * @ORM\Entity(repositoryClass="\Auth\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements UserInterface, IdentityInterface
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @see ZfcUser\Entity\User
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     * @see ZfcUser\Entity\User
     * @ORM\Column(name="username", type="string", length=255, nullable=false, unique=true)
     */
    protected $username = '';

    /**
     * @var string
     * @see ZfcUser\Entity\User
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    protected $password = '';

    /**
     * @var string
     * @see ZfcUser\Entity\User
     * @ORM\Column(name="email", type="string", length=255, nullable=false, unique=true)
     */
    protected $email = '';

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true, unique=false)
     */
    protected $firstName;

    /**
     * @var string
     * @ORM\Column(name="second_name", type="string", length=255, nullable=true, unique=false)
     */
    protected $secondName;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=true, unique=false)
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(name="home_page", type="string", length=255, nullable=true, unique=false)
     */
    protected $homePage;

    /**
     * @var integer
     * @ORM\Column(name="icq", type="integer", nullable=true, unique=true)
     */
    protected $icq;

    /**
     * @var bool
     * @ORM\Column(name="hide_email", type="boolean", options={"default"="false"})
     */
    protected $hideEmail = true;

    /**
     * @var bool
     * @ORM\Column(name="additional", type="text", length=1000, nullable=true)
     */
    protected $additional = true;

    /**
     * @var bool
     * @ORM\Column(name="interests", type="text", length=1000, nullable=true)
     */
    protected $interests = true;

    /**
     * @var bool
     * @ORM\Column(name="locale", type="text", length=2, nullable=false)
     */
    protected $locale = 'ru';

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Role")
     */
    protected $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function getState() {}
    public function setState($a) {}
    public function getDisplayName() {}
    public function setDisplayName($a) {}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * @param string $secondName
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getHomePage()
    {
        return $this->homePage;
    }

    /**
     * @param string $homePage
     */
    public function setHomePage($homePage)
    {
        $this->homePage = $homePage;
    }

    /**
     * @return int
     */
    public function getIcq()
    {
        return $this->icq;
    }

    /**
     * @param int $icq
     */
    public function setIcq($icq)
    {
        $this->icq = $icq;
    }

    /**
     * @return boolean
     */
    public function isHideEmail()
    {
        return $this->hideEmail;
    }

    /**
     * @param boolean $hideEmail
     */
    public function setHideEmail($hideEmail)
    {
        $this->hideEmail = $hideEmail;
    }

    /**
     * @return boolean
     */
    public function isAdditional()
    {
        return $this->additional;
    }

    /**
     * @param boolean $additional
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;
    }

    /**
     * @return boolean
     */
    public function isInterests()
    {
        return $this->interests;
    }

    /**
     * @param boolean $interests
     */
    public function setInterests($interests)
    {
        $this->interests = $interests;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @return \ArrayIterator|\Traversable|Role[]
     */
    public function getRolesIterator()
    {
        return $this->roles->getIterator();
    }

    /**
     * Set the list of roles
     * @param Collection $roles
     */
    public function setRoles(Collection $roles)
    {
        $this->roles->clear();
        foreach ($roles as $role) {
            $this->roles[] = $role;
        }
    }

    /**
     * Add one role to roles list
     * @param \Rbac\Role\RoleInterface $role
     */
    public function addRole(RoleInterface $role)
    {
        $this->roles[] = $role;
    }


}