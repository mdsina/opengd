<?php

namespace Auth\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use DoctrineExtensions\NestedSet\Config;
use DoctrineExtensions\NestedSet\Manager;
use DoctrineExtensions\NestedSet\NodeWrapper;
use Forum\Entity\ForumCategory;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * Class UserRepository
 * @package Auth\Repository
 */
class UserRepository extends EntityRepository
{

    public function getUserList($limit = 20, $offset = 0)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from('Auth\Entity\User', 'u')
            ->setMaxResults($limit)
            ->setFirstResult($offset);
        $q = $qb->getQuery();

        return $q->getResult(Query::HYDRATE_SCALAR);
    }
}
