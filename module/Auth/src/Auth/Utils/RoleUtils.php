<?php

namespace Auth\Utils;


use Auth\Entity\Role;

class RoleUtils
{
    /**
     * @param Role[] $roles
     * @param array $params (extract_type=`assoc|simple`)
     * @param int $depth
     * @return array
     */
    public static function getRolesPermissions(array $roles, array $params = [], $depth = 1)
    {
        if ($depth > Role::MAX_DEPTH) {
            return [];
        }

        $permissions = [];
        $permissionsExtractorType = isset($params['extract_type'])
            ? PermissionUtils::$typesMapping[$params['extract_type']]
            : PermissionUtils::EXTRACT_ARRAY_SIMPLE;

        foreach($roles as $role) {
            $permissions = array_merge(
                $permissions,
                PermissionUtils::extractPemissions($role->getPermissions()->toArray(), $permissionsExtractorType)
            );

            if ($role->hasChildren()) {
                $permissions = array_merge(
                    $permissions,
                    self::getRolesPermissions($role->getChildren()->toArray())
                );
            }
        }

        return $permissions;
    }
}