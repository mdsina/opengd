<?php

namespace Auth\Utils;


use Auth\Entity\Permission;

class PermissionUtils
{
    /**
     * Указываем возвращать список привилегий как обычный массив значений
     */
    const EXTRACT_ARRAY_SIMPLE = 0;

    /**
     * Указываем вернуть список привилегий в виде ассоциативного массива, где ключи берутся из \Auth\Enum\Permission
     */
    const EXTRACT_ARRAY_ASSOC = 1;

    static $typesMapping = [
        'assoc' => self::EXTRACT_ARRAY_ASSOC,
        'simple' => self::EXTRACT_ARRAY_SIMPLE,
    ];

    /**
     * @param Permission[] $permissions
     * @param int $extractType
     * @return array
     */
    public static function extractPemissions(array $permissions, $extractType = self::EXTRACT_ARRAY_SIMPLE)
    {
        $result = [];

        switch ($extractType) {
            case self::EXTRACT_ARRAY_ASSOC:
                $tempPermissions = \Auth\Enum\Permissions::getList();
                $result = array_filter($tempPermissions, function($permission) use (&$permissions) {
                    foreach ($permissions as $index => $value) {
                        if ($value == $permission) {
                            unset($permissions[$index]);

                            return $permission;
                        }
                    }
                });
                break;
            case self::EXTRACT_ARRAY_SIMPLE:
            default:
                foreach ($permissions as $permission) {
                    $result[] = (string)$permission;
                }
                break;
        }

        return $result;
    }
}