<?php

namespace Auth;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            'Auth\Controller\AuthController' => InvokableFactory::class
        ],
    ],

    'router' => [
        'routes' => [
            'auth' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/auth',
                    'defaults' => [
                        'controller' => 'Auth\Controller\Auth',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'get_current_permissions' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/get-current-permissions[/:display]',
                            'constraints' => [
                                'display' => '(simple|assoc)',
                            ],
                            'defaults' => [
                                'controller' => 'Auth\Controller\Auth',
                                'action' => 'getCurrentPermissions',
                            ],
                        ],
                    ],
                    'get_permissions_list' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/get-permissions-list',
                            'defaults' => [
                                'controller' => 'Auth\Controller\Auth',
                                'action' => 'getPermissionsList',
                            ],
                        ],
                    ],
                ],
            ],
        ]
    ],
];
