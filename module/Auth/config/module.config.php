<?php

namespace Auth;

$router = include(__DIR__ . '/router.config.php');
$permissions = include(__DIR__ . '/permissions.config.php');

return array_merge_recursive($router, $permissions, [
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Auth/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                    'ZfcUser\Entity' => 'zfcuser_entity'
                ],
            ],
            'zfcuser_entity' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => [__DIR__ . '/../src/Auth/Entity']
            ],
            'zfcuserdoctrineorm_entity' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Auth/Entity']
            ],
        ],
        'authentication' => [
            'orm_default' => [
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'Auth\Entity\User',
                'identity_property' => 'username',
                'credential_property' => 'password',
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'types' => [
                ],
            ],
        ],
    ],
    'zfcuser' => [
        'user_entity_class' => 'Auth\Entity\User',
        'userEntityClass' => 'Auth\Entity\User',
        'enable_default_entities' => false,
        'enable_username' => true,
        'enable_display_name' => false,
        'auth_identity_fields' => ['email', 'username'],
        'enable_user_state' => false,
        'use_registration_form_captcha' => false,
        'form_captcha_options' => [
            'class' => 'image',
            'options' => [
                'wordLen' => 5,
                'expiration' => 300,
                'width' => 160,
                'height' => 80,
                'timeout' => 300,
                'font' => __DIR__ . '/../config/carlisle_regular__2.ttf',
                'imgDir' => getcwd() . '/files/captcha/',
                'imgUrl' => '/captcha/',
                'dotNoiseLevel' => 40,
                'lineNoiseLevel' => 5
            ],
        ],
        'password_cost' => 14,
    ],
    'zfc_rbac' => [
        'identity_provider' => 'ZfcRbac\Identity\AuthenticationIdentityProvider',
        'role_provider' => [
            'ZfcRbac\Role\ObjectRepositoryRoleProvider' => [
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'class_name' => 'Auth\Entity\Role',
                'role_name_property' => 'name'
            ]
        ]
    ],
    'service_manager' => [
        'factories' => [
            'Zend\Authentication\AuthenticationService' => function ($serviceManager) {
                return $serviceManager->get('doctrine.authenticationservice.orm_default');
            }
        ],
    ],
]);
