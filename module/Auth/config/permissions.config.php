<?php

namespace Auth;

use Auth\Enum\Permissions;

return [
    'zfc_rbac' => [
        'guards' => [
            'ZfcRbacAdditions\Guard\ControllerAndActionPermissionsGuard' => [
                'Auth\Controller\Auth' => [
                    ['action' => 'get-current-permissions', 'permissions' => [Permissions::ADMIN_VIEW]],
                    ['action' => 'get-permissions-list', 'permissions' => [Permissions::ADMIN_VIEW]],
                ],
            ],
        ]
    ],
];
