<?php


namespace Application\View\Helper;


use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormLabel;

/**
 * Class FormDescription
 * @package Application\View\Helper
 */
class FormDescription extends FormLabel
{
    /**
     * @param ElementInterface $element
     * @param null $labelContent
     * @param null $position
     * @return string|FormLabel
     */
    public function __invoke(ElementInterface $element = null, $labelContent = null, $position = null)
    {
        if ($labelContent === null) {
            $labelContent = $element->getOption('description');
        }

        if ($labelContent !== null && null !== ($oTranslator = $this->getTranslator())) {
            $labelContent = $oTranslator->translate(
                $labelContent,
                $this->getTranslatorTextDomain()
            );
        }

        return parent::__invoke($element, $labelContent, $position);
    }
}