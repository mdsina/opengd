<?php

namespace Forum\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use DoctrineExtensions\NestedSet\Config;
use DoctrineExtensions\NestedSet\Manager;
use DoctrineExtensions\NestedSet\NodeWrapper;
use Forum\Entity\ForumCategory;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * Class ForumRepository
 * @package Forum\Repository
 */
class ForumRepository extends EntityRepository
{
    /**
     * @var Manager
     */
    protected $nsManager = null;

    /**
     * @return Manager
     */
    public function getManager()
    {
        if (!isset($this->nsManager)) {
            $config = new Config($this->getEntityManager(), $this->getClassName());
            $this->nsManager = new Manager($config);
        }

        return $this->nsManager;
    }

    /**
     * @param ForumCategory $liftedNode
     * @param ForumCategory $parentNode
     * @return ForumCategory
     * @throws \Exception
     */
    public function createChildCategory(ForumCategory $liftedNode, ForumCategory $parentNode)
    {
        $liftedNode->setParent($parentNode);
        $liftedNode->setDepth($parentNode->getDepth() + 1);
        $this->getManager()->wrapNode($parentNode)->addChild($liftedNode);

        return $liftedNode;
    }

    /**
     * @param ForumCategory $category
     * @param ForumCategory $parentNode
     * @return ForumCategory
     */
    public function createCategory(ForumCategory $category, ForumCategory $parentNode = null)
    {
        if (!$parentNode) {
            $parentNode = $category->getParent();
        }

        if (!$parentNode) {
            $this->getManager()->createRoot($category);
        } else {
            $this->createChildCategory($category, $parentNode);
        }

        return $category;
    }

    /**
     * @param ForumCategory $category
     */
    public function deleteCategory(ForumCategory $category)
    {
        $this->deleteNode($category);
    }

    /**
     * @param ForumCategory $item
     * @param bool $isEnableDescendants
     */
    public function enableItem(ForumCategory $item, $isEnableDescendants = true)
    {
        $this->enableNode($item, $isEnableDescendants);
    }

    /**
     * @param ForumCategory $category
     */
    private function deleteNode(ForumCategory $category)
    {
        $this->getManager()->wrapNode($category)->delete();
    }

    /**
     * @param ForumCategory $category
     * @param bool $isEnableDescendants
     */
    private function enableNode(ForumCategory $category, $isEnableDescendants)
    {
        $category->setDisabled(true);

        $flushStack = [];
        $flushStack[] = $category;

        if($isEnableDescendants) {
            /** @var NodeWrapper[] $descendants */
            $descendants = $this->getManager()->wrapNode($category)->getDescendants();
            foreach ($descendants as $descendant) {
                $flushStack[] = $descendant->getNode()->setDisabled(false);
            }
        }


        $this->getEntityManager()->flush($flushStack);
    }

    /**
     * @param ForumCategory $item
     */
    public function deleteItem(ForumCategory $item)
    {
        $this->deleteNode($item);
    }

    /**
     * @param ForumCategory $referenceNode
     * @return ForumRepository
     * @throws \Exception
     */
    public function deleteNodeHard(ForumCategory $referenceNode)
    {
        $this->getManager()->wrapNode($referenceNode)->delete();

        return $this;
    }


    /**
     * @param array $criteria
     * @return array
     */
    public function getUnpackedDescendantsByCriteria(array $criteria)
    {
        $data = $this->getDescendantsByCriteria($criteria);
        $data = $data->toArray();

        $hydrator = new ClassMethods();
        foreach ($data as &$item) {
            $item = $hydrator->extract($item);
        }

        return $data;
    }

    /**
     * @param array $criteria
     * @return ArrayCollection
     */
    public function getDescendantsByCriteria(array $criteria)
    {
        /** @var ForumCategory $categoryNode */
        return ($categoryNode = $this->findOneBy($criteria))
            ? $this->getDescendants($categoryNode)
            : new ArrayCollection();
    }

    /**
     * @param ForumCategory $category
     * @param bool $isIncludeDisabled
     * @param bool $isReturnCollection
     * @return ArrayCollection|array
     */
    public function getDescendants(ForumCategory $category, $isIncludeDisabled = false, $isReturnCollection = true)
    {
        /**
         * @var ForumCategory[] $output
         * @var NodeWrapper[] $descendants
         */
        $output = [];

        $descendants = $this->getManager()->wrapNode($category)->getDescendants();
        foreach ($descendants as $descendant) {
            if (!$isIncludeDisabled && $descendant->getNode()->isDisabled()) {
                continue;
            }
            $output[] = $descendant->getNode();
        }

        if ($isReturnCollection) {
            return $output;
        }

        return new ArrayCollection($output);
    }

    /**
     * @param string $parentName
     * @param string $childName
     * @return ForumCategory|null
     */
    public function getCategoryItemByNames($parentName, $childName)
    {
        /**
         * @var ForumCategory[] $catalog
         * @var ForumCategory $categoryNode
         */
        $catalog = ($categoryNode = $this->findOneBy(['code' => $parentName]))
            ? $this->getDescendants($categoryNode)
            : new ArrayCollection();
        foreach ($catalog as $item) {
            if ($item->getName() == $childName) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @param array $criteria
     * @param bool $isIncludeSelf
     * @return ArrayCollection
     */
    public function getSiblingsByCriteria(array $criteria, $isIncludeSelf = true)
    {
        /** @var ForumCategory $referenceNode */
        return ($categoryNode = $this->findOneBy($criteria))
            ? $this->getSiblings($categoryNode, $isIncludeSelf)
            : new ArrayCollection();
    }

    /**
     * @param ForumCategory $category
     * @param bool $isIncludeSelf
     * @param bool $isIncludeDisabled
     * @return ArrayCollection
     */
    public function getSiblings(ForumCategory $category, $isIncludeSelf = true, $isIncludeDisabled = false)
    {
        /**
         * @var ForumCategory[] $output
         * @var NodeWrapper[] $siblingWrappers
         */
        $output = [];

        $siblingWrappers = $this->getManager()->wrapNode($category)->getSiblings($isIncludeSelf);
        foreach ($siblingWrappers as $siblingWrapper) {
            if (!$isIncludeDisabled && $siblingWrapper->getNode()->isDisabled()) {
                continue;
            }
            $output[] = $siblingWrapper->getNode();
        }

        return new ArrayCollection($output);
    }

    /**
     * @return ForumCategory[]
     */
    public function fetchRoots()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('f')
            ->from(ForumCategory::class, 'f')
            ->where($qb->expr()->isNull('f.parent'));

        $res = $qb->getQuery()->getResult();

        if (empty($res)) {
            return [];
        }

        return $res;
    }
}
