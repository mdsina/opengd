<?php


namespace Forum\Controller;


use Forum\Form\ForumCreate;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return 'YOBA';
    }

    public function createAction()
    {
        /** @var ForumCreate $form */
        $form = $this->getServiceLocator()->get('FormElementManager')->get('ForumCreateForm');

        return new ViewModel(['form' => $form]);
    }
}