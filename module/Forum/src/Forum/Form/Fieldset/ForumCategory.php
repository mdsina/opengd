<?php


namespace Forum\Form\Fieldset;

use DoctrineExt\Mapper\EntityManagerAwareInterface;
use DoctrineExt\Mapper\EntityManagerAwareTrait;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Forum\Entity\ForumCategory as ForumCategoryEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;


class ForumCategory extends Fieldset implements InputFilterProviderInterface, EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /** элементы формы */
    const ELEMENT_ID = 'id';
    const ELEMENT_NAME = 'name';
    const ELEMENT_TITLE = 'title';
    const ELEMENT_DESCRIPTION = 'description';
    const ELEMENT_PARENT = 'parent';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->setHydrator(new DoctrineObject(
            $this->getEntityManager(),
            '\Forum\Entity\ForumCategory'
        ))->setObject(new ForumCategoryEntity());


        $this->add([
            'name' => self::ELEMENT_ID,
            'type' => 'Zend\Form\Element\Number',
            'options' => [
                'hidden' => true,
            ],
            'attributes' => [
                'min' => '0',
                'id' => self::ELEMENT_ID,
            ],
        ]);

        $this->add([
            'name' => self::ELEMENT_NAME,
            'type' => 'Zend\Form\Element\Text',
            'options' => [
                'label' => 'Name',
                'description' => 'Unique name likes "some_unique_name". If empty will be generated from Title',
                'column-size' => 'sm-10',
                'label_attributes' => ['class' => 'col-sm-2']
            ],
            'attributes' => [
                'id' => self::ELEMENT_NAME,
            ],
        ], [
            'priority' => 30
        ]);

        $this->add([
            'name' => self::ELEMENT_TITLE,
            'type' => 'Zend\Form\Element\Text',
            'options' => [
                'label' => 'Title',
                'description' => 'The title of forum',
                'column-size' => 'sm-10',
                'label_attributes' => ['class' => 'col-sm-2']
            ],
            'attributes' => [
                'id' => self::ELEMENT_TITLE,
            ],
        ], [
            'priority' => 20
        ]);

        $this->add([
            'name' => self::ELEMENT_DESCRIPTION,
            'type' => 'Zend\Form\Element\Text',
            'options' => [
                'label' => 'Description',
                'description' => 'Description of forum',
                'column-size' => 'sm-10',
                'label_attributes' => ['class' => 'col-sm-2']
            ],
            'attributes' => [
                'id' => self::ELEMENT_DESCRIPTION,
            ],
        ], [
            'priority' => 10
        ]);

        $this->add([
            'type'    => 'DoctrineModule\Form\Element\ObjectSelect',
            'name'    => self::ELEMENT_PARENT,
            'options' => [
                'column-size' => 'sm-10',
                'label_attributes' => ['class' => 'col-sm-2'],
                'label'          => 'Parent forum',
                'object_manager' => $this->getEntityManager(),
                'target_class'   => ForumCategoryEntity::class,
                'property'       => 'title',
                'empty_option'   => '---',
                'find_method'   => ['name' => 'fetchTree'],
                'label_generator' => function ($entity) {
                    if (!($entity instanceof ForumCategoryEntity)) {
                        return null;
                    }

                    $title = $entity->getTitle();

                    if (empty($title)) {
                        $title = $entity->getName();
                    }

                    return str_repeat('--', $entity->getDepth()) . $title;
                },
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getInputFilterSpecification()
    {
        return [
            self::ELEMENT_ID => [
                'required' => false,
                'filters' => [
                    ['name' => 'Digits'],
                    ['name' => 'ToNull'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                        'options' => ['min' => 0],
                    ],
                    [
                        'name' => 'DoctrineExt\Validator\ObjectExists',
                        'options' => [
                            'object_repository' => $this->getEntityManager()->getRepository(ForumCategoryEntity::class),
                            'fields' => [self::ELEMENT_ID],
                        ],
                    ],
                ],
            ],
            self::ELEMENT_NAME => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => ['min' => 3, 'max' => 127],
                    ],
                    [
                        'name' => 'Regex',
                        'options' => ['pattern' => '/^[\w-]+/'],
                    ],
                    [
                        'name' => 'DoctrineExt\Validator\NoObjectExists',
                        'options' => [
                            'object_repository' => $this->getEntityManager()->getRepository(ForumCategoryEntity::class),
                            'fields' => [self::ELEMENT_NAME],
                        ],
                    ],
                ],
            ],
            self::ELEMENT_PARENT => [
                'required' => false,
                'filters' => [
                    ['name' => 'Digits'],
                    ['name' => 'ToNull'],
                ],
                'validators' => [
                    [
                        'name' => 'DoctrineExt\Validator\ObjectExists',
                        'options' => [
                            'object_repository' => $this->getEntityManager()->getRepository(ForumCategoryEntity::class),
                            'fields' => [self::ELEMENT_ID],
                        ],
                    ],
                ],
            ],
            self::ELEMENT_TITLE => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => ['min' => 1, 'max' => 256],
                    ],
                ],
            ],
            self::ELEMENT_DESCRIPTION => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'StringLength',
                        'options' => ['min' => 1, 'max' => 256],
                    ],
                ],
            ],
        ];
    }
}
