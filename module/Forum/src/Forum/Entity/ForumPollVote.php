<?php

namespace Forum\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="forum_poll_votes")
 */
class ForumPollVote
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Auth\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Auth\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     **/
    protected $user;

    /**
     * @var ForumTopic
     *
     * @ORM\ManyToOne(targetEntity="ForumTopic")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", nullable=false)
     **/
    protected $topic;

    /**
     * @var ForumPollOption
     *
     * @ORM\ManyToOne(targetEntity="ForumPollOption")
     * @ORM\JoinColumn(name="poll_option_id", referencedColumnName="id", nullable=false)
     **/
    protected $pollOption;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Auth\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \Auth\Entity\User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return ForumTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param ForumTopic $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return ForumPollOption
     */
    public function getPollOption()
    {
        return $this->pollOption;
    }

    /**
     * @param ForumPollOption $pollOption
     */
    public function setPollOption($pollOption)
    {
        $this->pollOption = $pollOption;
    }
}
