<?php

namespace Forum\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="forum_topics")
 */
class ForumTopic
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Auth\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Auth\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     **/
    protected $owner;

    /**
     * @var ForumCategory
     *
     * @ORM\ManyToOne(targetEntity="ForumCategory")
     * @ORM\JoinColumn(name="forum_id", referencedColumnName="id", nullable=false)
     **/
    protected $forumCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", length=5000, nullable=false)
     */
    protected $title;

    /**
     * @var bool
     *
     * @ORM\Column(name="important", type="boolean", nullable=false)
     */
    protected $important;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=false)
     */
    protected $time;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=false)
     */
    protected $lastUpdate;


}
