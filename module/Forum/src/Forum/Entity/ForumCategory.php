<?php

namespace Forum\Entity;

use Auth\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DoctrineExtensions\NestedSet\MultipleRootNode;

/**
 * @ORM\Entity(repositoryClass="\Forum\Repository\ForumRepository")
 * @ORM\Table(name="forums")
 */
class ForumCategory implements MultipleRootNode
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=256, nullable=false, unique=true)
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=256, nullable=false)
     */
    protected $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=256)
     */
    protected $description;

    /**
     * @var ForumCategory|null
     *
     * @ORM\ManyToOne(targetEntity="ForumCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @var ForumCategory[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ForumCategory", mappedBy="parent")
     */
    protected $children;

    /**
     * @var integer
     *
     * @ORM\Column(name="lft", type="integer", options={"comment"="Nested sets. Lft"})
     */
    protected $lft = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="rgt", type="integer", options={"comment"="Nested sets. Rgt"})
     */
    protected $rgt = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="root_id", type="integer", options={"comment"="Nested sets. Root"})
     */
    protected $root = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_disabled", type="boolean", options={"default"="false", "comment":"Удалена ли запись"})
     */
    protected $disabled = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_manually_created", type="boolean", options={"default"="true", "comment":"Создан форум вручную или автоматически"})
     */
    protected $manuallyCreated = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="depth", type="integer", options={"comment"="depth of tree"})
     */
    protected $depth = 0;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="\Auth\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $owner;


    /**
     * Init the Doctrine collection
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $child
     */
    public function addChild($child)
    {
        $this->children[] = $child;
    }

    /**
     * @return ArrayCollection|ForumCategory[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return !$this->children->isEmpty();
    }

    /**
     * @return ForumCategory|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ForumCategory $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * sets Node's root value
     *
     * @param ForumCategory $root
     */
    public function setRootValue($root)
    {
        $this->root = $root;
    }

    /**
     * gets Node's root value
     *
     * @return ForumCategory|null
     */
    public function getRootValue()
    {
        return $this->root;
    }

    /**
     * sets Node's left value
     *
     * @param int $lft
     */
    public function setLeftValue($lft)
    {
        $this->lft = $lft;
    }

    /**
     * gets Node's left value
     *
     * @return int
     */
    public function getLeftValue()
    {
        return $this->lft;
    }

    /**
     * sets Node's right value
     *
     * @param int $rgt
     */
    public function setRightValue($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * gets Node's right value
     *
     * @return int
     */
    public function getRightValue()
    {
        return $this->rgt;
    }

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param boolean $disabled
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }

    /**
     * @return int
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param int $depth
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    /**
     * @return boolean
     */
    public function isManuallyCreated()
    {
        return $this->manuallyCreated;
    }

    /**
     * @param boolean $manuallyCreated
     */
    public function setManuallyCreated($manuallyCreated)
    {
        $this->manuallyCreated = $manuallyCreated;
    }

    /**
     * @return User|null
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User|null $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }
}
