<?php

namespace Forum\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="forum_poll_options")
 */
class ForumPollOption
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var ForumTopic
     *
     * @ORM\ManyToOne(targetEntity="ForumTopic")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", nullable=false)
     **/
    protected $topic;

    /**
     * @var string
     *
     * @ORM\Column(name="option_text", type="string", length=65, nullable=false)
     **/
    protected $optionText = "";

    /**
     * @var integer
     *
     * @ORM\Column(name="option_total", type="integer", nullable=false)
     **/
    protected $optionTotal = 0;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return ForumTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param ForumTopic $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return string
     */
    public function getOptionText()
    {
        return $this->optionText;
    }

    /**
     * @param string $optionText
     */
    public function setOptionText($optionText)
    {
        $this->optionText = $optionText;
    }

    /**
     * @return int
     */
    public function getOptionTotal()
    {
        return $this->optionTotal;
    }

    /**
     * @param int $optionTotal
     */
    public function setOptionTotal($optionTotal)
    {
        $this->optionTotal = $optionTotal;
    }
}
