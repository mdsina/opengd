<?php

namespace Forum\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="forum_posts")
 */
class ForumPost
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Auth\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Auth\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     **/
    protected $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=5000, nullable=false)
     */
    protected $content;

    /**
     * @var ForumTopic
     *
     * @ORM\ManyToOne(targetEntity="ForumTopic")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", nullable=false)
     **/
    protected $topic;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=false)
     */
    protected $time;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edit_time", type="datetime")
     */
    protected $editTime;

    /**
     * @var \Auth\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Auth\Entity\User")
     * @ORM\JoinColumn(name="edit_user_id", referencedColumnName="id")
     **/
    protected $editUser;

    /**
     * @var boolean
     *
     * @ORM\Column(name="edit_locked", type="boolean", nullable=false)
     **/
    protected $editLocked = false;

    /**
     * @var string
     *
     * @ORM\Column(name="edit_reason", type="text", length=1000, nullable=false)
     **/
    protected $editReason = "";

    /**
     * @var string
     *
     * @ORM\Column(name="owner_ip", type="string", length=15, nullable=false)
     **/
    protected $ownerIp = "0.0.0.0";

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Auth\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param \Auth\Entity\User $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return ForumTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param ForumTopic $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return \DateTime
     */
    public function getEditTime()
    {
        return $this->editTime;
    }

    /**
     * @param \DateTime $editTime
     */
    public function setEditTime($editTime)
    {
        $this->editTime = $editTime;
    }

    /**
     * @return \Auth\Entity\User
     */
    public function getEditUser()
    {
        return $this->editUser;
    }

    /**
     * @param \Auth\Entity\User $editUser
     */
    public function setEditUser($editUser)
    {
        $this->editUser = $editUser;
    }

    /**
     * @return boolean
     */
    public function isEditLocked()
    {
        return $this->editLocked;
    }

    /**
     * @param boolean $editLocked
     */
    public function setEditLocked($editLocked)
    {
        $this->editLocked = $editLocked;
    }

    /**
     * @return string
     */
    public function getEditReason()
    {
        return $this->editReason;
    }

    /**
     * @param string $editReason
     */
    public function setEditReason($editReason)
    {
        $this->editReason = $editReason;
    }
}
