<?php

namespace Forum;

use Auth\Enum\Permissions;

return [
    'zfc_rbac' => [
        'guards' => [
            'ZfcRbacAdditions\Guard\ControllerAndActionPermissionsGuard' => [
                'Forum\Controller\Index' => [
                    ['action' => 'create', 'permissions' => [Permissions::ADMIN_FORUMS_ADD]],
                ],
            ],
        ]
    ],
];
