<?php

namespace Forum;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            'Forum\Controller\IndexController' => InvokableFactory::class,
        ],
    ],

    'router' => [
        'routes' => [
            'forum' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/forum[/:action][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => 'Forum\Controller\Index',
                        'action' => 'index',
                    ],
                ],
            ],
        ]
    ],
];
