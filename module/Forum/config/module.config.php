<?php

namespace Forum;

$router = include(__DIR__ . '/router.config.php');
$permissions = include(__DIR__ . '/permissions.config.php');

return array_merge($router, $permissions, [
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Forum/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],

    'translator' => [
        'locale' => 'ru',
        'translation_file_patterns' => [
            [
                'type' => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => 'lang.array.%s.php',
            ],
        ],
    ],

    'service_manager' => [
        'invokables' => [
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
]);
