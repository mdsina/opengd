<?php

return [
    'Create forum' => 'Создать форум',

    'Name' => 'Имя',
    'Title' => 'Заголовок',
    'Description' => 'Описание',

    'Unique name likes "some_unique_name". If empty will be generated from Title' => 'Уникальное имя "some_unique_name". Если не заполнено, генерируется автоматически из Заголовка',
    'The title of forum' => 'Заголовок форума',
    'Description of forum' => 'Описание форума',

    'Submit' => 'Отправить',

];
