<?php

namespace DoctrineExt;

use DoctrineExt\Mapper\EntityManagerAwareInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Console\Adapter\AdapterInterface as Console;

/**
 * Class Module
 * @package Core
 */
class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ConsoleBannerProviderInterface,
    ConsoleUsageProviderInterface,
    FormElementProviderInterface
{
    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceConfig()
    {
        return [
            'initializers' => [
                'DoctrineExt\Initializer\EntityManagerInitializer',
            ],
        ];
    }

    public function getConsoleBanner(Console $console)
    {
        return 'Doctrine Module';
    }

    public function getConsoleUsage(Console $console)
    {
        return [
            'doctrine' => 'Show doctrine commands list',
            'doctrine [<command>]' => 'Execute any doctrine usual command with literal parameters, flags, value parameters',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormElementConfig()
    {
        return [
            'initializers' => [
                'DoctrineExt\EntityManager' => function ($form, $serviceManager) {
                    if ($form instanceof EntityManagerAwareInterface) {
                        $form->setEntityManager(
                            $serviceManager->getServiceLocator()->get('Doctrine\ORM\EntityManager')
                        );
                    }
                },
            ],
        ];
    }
}
