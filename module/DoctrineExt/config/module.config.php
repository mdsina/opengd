<?php

namespace DoctrineExt;

use Core\Grid\BasePaginatorGrid;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\ServiceManager\ServiceManager;

$options = [
    // common
    '--help', '-h', '--quite', '-q', '--verbose', '-v|-vv|-vvv', '--version', '-V', '--ansi', '--no-ansi', '--no-interaction', '-n',
    // help
    '--xml', '--format=', '--raw',
    // dbal:run-sql
    '--depth=',
    // migrations common
    '--configuration', '--configuration=', '--db-configuration', '--db-configuration=',
    // migrations:generate
    '--editor-cmd', '--editor-cmd=',
    // migrations:diff
    '--editor-cmd', '--editor-cmd=', '--filter-expression', '--filter-expression=',
    // mirgations:version
    '--add', '--delete', '--all',
    // migrations:execute
    '--write-sql', '--dry-run', '--up|--down',
    // migrations:migrate
    '--write-sql', '--dry-run',
    // migrations:status
    '--show-versions',
    // orm:clear-cache:metadata, orm:clear-cache:query, orm:clear-cache:result
    '--flush',
    // orm:convert-d1-schema
    '--from=', '--extend', '--extend=', '--num-spaces', '--num-spaces=',
    // orm:convert-mapping
    '--filter=', '--force', '--from-database', '--extend', '--extend=', '--num-spaces', '--num-spaces=', '--namespace', '--namespace=',
    // orm:ensure-production-settings
    '--complete',
    // orm:generate-entities
    '--filter=', '--generate-annotations', '--generate-annotations=', '--generate-methods', '--generate-methods=', '--regenerate-entities', '--regenerate-entities=', '--update-entities', '--update-entities=', '--extend', '--extend=', '--num-spaces', '--num-spaces=',
    // orm:generate-proxies, orm:generate:repositories
    '--filter=',
    // orm:run-dql
    '--hydrate=', '--first-result=', '--max-result=', '--depth=',
    // orm:schema-tool:create
    '--dump-sql',
    // orm:schema-tool:drop
    '--dump-sql', '--force', '--full-database',
    // orm:schema-tool:update
    '--complete', '--dump-sql', '--force',
];
$optionsUnique = '[' . implode('] [', array_unique($options)) . ']';

return [
    'console' => [
        'router' => [
            'routes' => [
                'doctrine' => [
                    'options' => [
                        'route' => 'doctrine [<p1>] [<p2>] [<p3>] [<p4>] ' . $optionsUnique,
                        'defaults' => [
                            'controller' => 'DoctrineExt\Controller\Doctrine',
                            'action' => 'index',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            'DoctrineExt\Controller\DoctrineController' => InvokableFactory::class,
        ],
    ],
];
