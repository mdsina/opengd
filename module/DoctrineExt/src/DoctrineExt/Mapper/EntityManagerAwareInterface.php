<?php

namespace DoctrineExt\Mapper;

use Doctrine\ORM\EntityManager;

/**
 * Interface EntityManagerAwareInterface
 * @package DoctrineExt\Mapper
 */
interface EntityManagerAwareInterface
{
    /**
     * @return EntityManager
     */
    public function getEntityManager();

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @return void
     */
    public function setEntityManager(EntityManager $entityManager);
}
