<?php

namespace DoctrineExt\Controller;

use Symfony\Component\Console\Input\ArgvInput;
use Zend\Mvc\Controller\AbstractActionController;

class DoctrineController extends AbstractActionController
{
    public function indexAction()
    {
        /* @var $cli \Symfony\Component\Console\Application */
        $cli = $this->getServiceLocator()->get('doctrine.cli');
        $cli->run($this->getDoctrineArgv());
    }

    /**
     * @return ArgvInput
     */
    public function getDoctrineArgv()
    {
        $dir = explode(DIRECTORY_SEPARATOR, __DIR__);
        do {
            $popDir = array_pop($dir);
        } while ($popDir != 'src');

        $serverArgv = $_SERVER['argv'];
        $searchIndex = array_search('doctrine', $serverArgv);
        if (!is_bool($searchIndex)) {
            unset($serverArgv[$searchIndex]);
        }

        $argvInput = new ArgvInput($serverArgv);

        return $argvInput;
    }
}
