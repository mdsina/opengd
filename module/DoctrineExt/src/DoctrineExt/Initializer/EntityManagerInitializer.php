<?php

namespace DoctrineExt\Initializer;

use DoctrineExt\Mapper\EntityManagerAwareInterface;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Initializer\InitializerInterface;

class EntityManagerInitializer implements InitializerInterface
{
    /**
     * Initialize
     *
     * @param \Interop\Container\ContainerInterface $container
     * @param mixed $instance
     */
    public function __invoke(ContainerInterface $container, $instance)
    {
        if ($instance instanceof EntityManagerAwareInterface) {
            $instance->setEntityManager($container->get('Doctrine\ORM\EntityManager'));
        }
    }
}
