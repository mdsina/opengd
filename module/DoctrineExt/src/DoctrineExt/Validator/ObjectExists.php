<?php


namespace DoctrineExt\Validator;


class ObjectExists extends \DoctrineModule\Validator\ObjectExists
{
    /**
     * @param string $messageKey
     * @param array|object|string $value
     * @return string
     */
    protected function createMessage($messageKey, $value)
    {
        if (!empty($value) && is_array($value) && count($value) == 1) {
            $value = array_shift($value);
        }

        return parent::createMessage($messageKey, $value);
    }
}