<?php


namespace DoctrineExt\Validator;


class NoObjectExists extends \DoctrineModule\Validator\NoObjectExists
{
    /**
     * @param string $messageKey
     * @param array|object|string $value
     * @return string
     */
    protected function createMessage($messageKey, $value)
    {
        if (!empty($value) && is_array($value) && count($value) == 1) {
            $value = array_shift($value);
        }

        return parent::createMessage($messageKey, $value);
    }
}