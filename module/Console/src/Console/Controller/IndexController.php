<?php

namespace Console\Controller;

use Auth\Enum\Permissions;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Zend\Console\Request as ConsoleRequest;

class IndexController extends AbstractActionController
{
    private static $jsdocStart =    "/**\n";
    private static $jsdocTypedef =  " * @typedef {Object} %s\n";
    private static $jsdocPattern =  " * @property {String} %s\n";
    private static $jsdocEnd =      " */\n";

    private static $jsdocFilePath = "public/js/app/jsdoc.js";

    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * {@inheritdoc}
     */
    public function onDispatch(MvcEvent $e)
    {
        $request = $this->getRequest();

        if (!$request instanceof ConsoleRequest){
            throw new \RuntimeException('You can only use this action from a console!');
        }

        return parent::onDispatch($e);
    }

    public function jsdocAction()
    {
        $request = $this->getRequest();

        $generateDocs   = $request->getParam('mode', 'generate');
        $verbose     = $request->getParam('verbose') || $request->getParam('v');

        if (!$generateDocs) {
            return "Please, specify param";
        }

        $permissionsJsdoc = $this->generatePermissionsJsdoc();
        $file = realpath(getcwd() . '/' .self::$jsdocFilePath);

        if ($verbose) {
            echo $permissionsJsdoc;
            echo $file;
        }

        $handle = fopen($file, "w");
        fwrite($handle, $permissionsJsdoc);
        fclose($handle);
    }

    private function generatePermissionsJsdoc()
    {
        $result = self::$jsdocStart . sprintf(self::$jsdocTypedef, 'Gdru.utils.Permissions');

        foreach (Permissions::getList() as $key => $value) {
            $result .= sprintf(self::$jsdocPattern, $key);
        }

        $result .= self::$jsdocEnd;

        return $result;
    }
}