<?php

namespace Console;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'console' => [
        'router' => [
            'routes' => [
                'jsdoc-generator' => [
                    'options' => [
                        'route' => 'jsdoc [generate]:mode [--verbose|-v]',
                        'defaults' => [
                            'controller' => 'Console\Controller\Index',
                            'action' => 'jsdoc'
                        ]
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            'Console\Controller\IndexController' => InvokableFactory::class,
        ],
    ],
];
