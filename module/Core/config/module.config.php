<?php

namespace Core;

use Core\Grid\BasePaginatorGrid;
use Zend\ServiceManager\ServiceManager;

return [
    'controllers' => [
        'initializers' => [
            'Core\Initializer\TranslatorAwareInitializer',
        ],
    ],

    'controller_plugins' => [
        'invokables' => [
            'PrepareFormPlugin' => 'Core\Controller\Plugin\PrepareFormPlugin',
        ]
    ],

    'input_filters' => [
        'invokables' => [
            'Core\InputFilter\GridInputFilter',
        ]
    ],

    'service_manager' => [
        'factories' => [
            'Core\Grid\BasePaginatorGrid' => function (ServiceManager $serviceManager) {
                return new BasePaginatorGrid();
            },
        ]
    ],
];
