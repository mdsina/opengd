<?php
namespace Core\Initializer;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Initializer\InitializerInterface;

class TranslatorAwareInitializer implements InitializerInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(ContainerInterface $container, $instance)
    {
        if ($instance instanceof \Zend\I18n\Translator\TranslatorAwareInterface) {
            $translationService = $container->get('MvcTranslator');
            $instance->setTranslator($translationService);
        }
    }
}
