<?php

namespace Core\Validator;

class PageNumber
{
    public static function get($paramName)
    {
        $input = new \Zend\InputFilter\Input($paramName);
        $input->getValidatorChain()
            ->attachByName('digits')
            ->attach(new \Zend\Validator\NotEmpty(array('integer', 'zero')));
        return $input;
    }
}
