<?php

namespace Core\Grid;


use Core\Tools\Pagination\Paginator;

interface PaginatorGridInterface
{
    /**
     * @return Paginator
     */
    public function getPaginator();

    /**
     * @param Paginator $paginator
     * @return void
     */
    public function setPaginator(Paginator $paginator);
}