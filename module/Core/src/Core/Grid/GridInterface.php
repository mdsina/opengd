<?php

namespace Core\Grid;


interface GridInterface
{
    public function setQuery($query);

    /**
     * @param array $filterMap
     */
    public function setFilterMap(array $filterMap);

    /**
     * @param array $sortMap
     */
    public function setSortMap($sortMap);

    /**
     * @return array
     */
    public function getFilterMap();
}