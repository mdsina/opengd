<?php

namespace Core\Grid;


use Core\Tools\Pagination\Paginator;

trait PaginatorGridTrait
{
    /**
     * @var Paginator
     */
    private $paginator = null;

    /**
     * @param Paginator $paginator
     */
    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @return Paginator
     */
    public function getPaginator()
    {
        return $this->paginator;
    }
}