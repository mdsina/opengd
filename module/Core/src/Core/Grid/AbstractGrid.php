<?php

namespace Core\Grid;

/**
 * Class AbstractGrid
 *
 * Абстрактный грид, создан исключительно для обазательной реализации интерфейса грида и объявления базового функционала
 * через реализацию конкретного типа грида. В качестве основы можно закладывать для использования как ORM так и чистым
 * SQL.
 *
 * @package Core\Grid
 */
abstract class AbstractGrid implements GridInterface
{
    use GridTrait;

    /**
     * Основной метод получения грида. Все операции по формированию SQL запроса проходят здесь.
     *
     * @param array $params - основной список параметров биндинга типа [':param1' => 'value'], параметры должны быть в
     * запросе
     * @param array $extraParams - дополнительные параметры, которые могут использоваться при реализации
     * @return mixed
     */
    public abstract function getList($params = [], $extraParams = []);
    protected abstract function prepareQuery();
    protected abstract function prepareWhere();
    protected abstract function prepareOrderBy();
    protected abstract function prepareLimit();
}