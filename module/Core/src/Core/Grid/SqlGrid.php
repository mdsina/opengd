<?php

namespace Core\Grid;

use DoctrineExt\Mapper\EntityManagerAwareInterface;
use DoctrineExt\Mapper\EntityManagerAwareTrait;
use Core\Tools\Pagination\Utils;
use Core\Tools\Query\Dml;
use Core\Tools\Query\Filter\Sql;

/**
 * Базовый класс грида для работы с чистыми SQL запросами, предпочиттельно использование для запросов PostgreSQL
 * (использует оконную функцию OVER())
 *
 * Class SqlGrid
 * @package Core\Grid
 */
class SqlGrid extends AbstractGrid implements PaginatorGridInterface, EntityManagerAwareInterface
{
    use PaginatorGridTrait, EntityManagerAwareTrait;

    /**
     * Может быть перегружен с инициализацией через setQuery и setFilterMap
     */
    public function __construct()
    {
    }

    /**
     * @param array $params
     *
     * @param array $extraParams
     * @return array
     */
    public function getList($params = [], $extraParams = [])
    {
        $this->prepareQuery();

        if (empty($this->query)) {
            return [
                'time' => 0,
                'total' => 0,
                'data' => [],
            ];
        }

        list($runTime, list($list, $totalCount)) = Utils::timeMeasurable(function () use ($params) {
            $connection = $this->getEntityManager()->getConnection();
            $connection->beginTransaction();

            if (is_array($params)) {
                $this->queryParameters = array_merge($this->queryParameters, $params);
            }
            $list = $connection->fetchAll($this->query, $this->queryParameters);
            $connection->commit();

            if (count($list) > 0) {
                $totalCount = $list[0]['total_count'];
            } else {
                $totalCount = 0;
            }

            return [$list, $totalCount];
        });

        return [
            'time' => $runTime,
            'total' => $totalCount,
            'data' => Dml::camelizeKeys($list),
        ];
    }

    /**
     * Подготавливаем запрос
     */
    protected function prepareQuery()
    {
        $this->wrapQueryToAnother();
        $this->prepareWhere();
        $this->prepareOrderBy();
        $this->prepareLimit();
    }

    /**
     * Оборачиваем запрос в основной с подсчетом количества элементов
     */
    protected function wrapQueryToAnother()
    {
        $unwrappedQuery = $this->query;
        $this->query = "SELECT  *,  count(*)  OVER () AS total_count FROM ( $unwrappedQuery ) as total_data";
    }

    /**
     * @return string
     */
    protected function prepareWhere()
    {
        $this->query = Sql::addConditions(
            $this->query,
            $this->getPaginator(),
            $this->filtersMap,
            true
        );
    }

    /**
     * @return string
     */
    protected function prepareOrderBy()
    {
        $this->query = Sql::addOrder(
            $this->query,
            $this->getPaginator(),
            empty($this->sortMap) ? $this->filtersMap : $this->sortMap
        );
    }

    /**
     * @return string
     */
    protected function prepareLimit()
    {
        $this->queryParameters[':limit'] = (int)$this->getPaginator()->getLimit();
        $this->queryParameters[':offset'] = (int)$this->getPaginator()->getOffset();

        $this->query .= ' LIMIT :limit OFFSET :offset';
    }
}
