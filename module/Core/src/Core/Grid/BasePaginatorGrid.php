<?php

namespace Core\Grid;


use Core\Tools\Pagination\Paginator;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Class BasePaginatorGrid
 *
 * Вспомогательный класс-фабрика, формирующий пагинатор, фильтр для входных данных и применяет к объекту самого грида.
 * Умеет кешировать параметры при получении данных, а так же сам пагинатор и фильтр.
 *
 * @package Core\Grid
 */
class BasePaginatorGrid implements PaginatorGridInterface, ServiceLocatorAwareInterface
{
    use PaginatorGridTrait, ServiceLocatorAwareTrait;

    protected $validateErrors = [];
    protected $listData = [];
    protected $paramsCache;

    /**
     * @var PaginatorGridInterface|AbstractGrid
     */
    protected $grid;

    /**
     * @var \Zend\InputFilter\InputFilter
     */
    protected $inputFilter;

    /**
     * @param string $class - полная ссылка на класс, реализующая PaginatorGridInterface
     * @param array $data - входные параметры в виде сортировки и фильтров
     *
     * @return bool - реузультат операции, true - данные валидны, false - есть ошибки, результат в validateErrors
     */
    public function prepare($class, $data)
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->setData($data);

        if (!$inputFilter->isValid()) {
            $this->validateErrors = $inputFilter->getMessages();

            return false;
        }

        $paginator = $this->getPaginator();
        $paginator->prepare($inputFilter);

        /** @var PaginatorGridInterface|AbstractGrid $grid */
        $this->grid = $this->getServiceLocator()->get($class);
        $this->grid->setPaginator($paginator);

        return true;
    }

    /**
     * @return Paginator
     */
    public function getPaginator()
    {
        if ($this->paginator) {
            return $this->paginator;
        }

        $this->paginator = new Paginator();

        return $this->paginator;
    }

    /**
     * @return \Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $this->inputFilter = $this->getServiceLocator()->get('InputFilterManager')
            ->get('Core\InputFilter\GridInputFilter');

        return $this->inputFilter;
    }

    /**
     * @param \Zend\InputFilter\InputFilter $inputFilter
     */
    public function setInputFilter($inputFilter)
    {
        $this->inputFilter = $inputFilter;
    }

    /**
     * @return array
     */
    public function getPrepareErrors()
    {
        return $this->validateErrors;
    }

    /**
     * @param array $params
     * @param array $extraParams
     * @return array
     */
    public function getList(array $params = [], $extraParams = [])
    {
        if ($this->paramsCache !== $params || $this->paramsCache === null) {
            $this->listData = $this->grid->getList($params, $extraParams);
            $this->paramsCache = $params;
        }

        return $this->listData;
    }
}