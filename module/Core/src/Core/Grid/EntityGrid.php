<?php

namespace Core\Grid;

use DoctrineExt\Mapper\EntityManagerAwareInterface;
use DoctrineExt\Mapper\EntityManagerAwareTrait;
use Core\Tools\Pagination\Utils;
use Core\Tools\Query\Dml;
use Doctrine\ORM\Query;

/**
 * Class EntityGrid
 *
 * Базовый класс грида для работы Doctrine ORM, конкретно с QueryBuilder. Не стоит использовать в конструкторе
 * EntityManager, т.к на этом этапе данная зависимость еще не определена и определяется Зендом после этапа создания
 * через Interface Injection. (см. \DoctrineExt\Initializer\EntityManagerInitializer)
 *
 * @package Core\Grid
 */
class EntityGrid extends AbstractGrid implements PaginatorGridInterface, EntityManagerAwareInterface
{
    use PaginatorGridTrait, EntityManagerAwareTrait;

    const DEFAULT_HYDRATION_MODE = Query::HYDRATE_SCALAR;

    /**
     * Может быть перегружен с инициализацией через setQuery и setFilterMap
     */
    public function __construct()
    {
    }

    /**
     * @param array $params
     * @param array $extraParams
     * @return array
     */
    public function getList($params = [], $extraParams = ['mode' => Query::HYDRATE_SCALAR])
    {
        $this->prepareQuery();

        if (empty($this->query)) {
            return [
                'time' => 0,
                'total' => 0,
                'data' => [],
            ];
        }

        if (empty($extraParams) || !isset($extraParams['mode'])) {
            $mode = self::DEFAULT_HYDRATION_MODE;
        } else {
            $mode = $extraParams['mode'];
        }

        list($runTime, list($list, $totalCount)) = Utils::timeMeasurable(function () use ($mode, $params) {
            if (is_array($params)) {
                $this->queryParameters = array_merge($this->queryParameters, $params);
            }

            $this->query->getQuery()->setParameters($this->queryParameters)
                ->setHydrationMode($mode);

            $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($this->query->getQuery());
            $paginator->setUseOutputWalkers(false);
            $list = iterator_to_array($paginator->getIterator());
            $totalCount = count($list);

            return [$list, $totalCount];
        });

        return [
            'time' => $runTime,
            'total' => $totalCount,
            'data' => Dml::camelizeKeys($list),
        ];
    }

    /**
     * @return void
     */
    protected function prepareQuery()
    {
        $this->prepareWhere();
        $this->prepareOrderBy();
        $this->prepareLimit();
    }

    /**
     * @return void
     */
    protected function prepareWhere()
    {
        \Core\Tools\Query\Filter\QueryBuilder::addConditions(
            $this->query,
            $this->getPaginator(),
            $this->filtersMap
        );
    }

    /**
     * @return void
     */
    protected function prepareOrderBy()
    {
        \Core\Tools\Query\Filter\QueryBuilder::addOrder(
            $this->query,
            $this->getPaginator(),
            empty($this->sortMap) ? $this->filtersMap : $this->sortMap
        );
    }

    /**
     * @return void
     */
    protected function prepareLimit()
    {
        $this->queryParameters[':limit'] = (int)$this->getPaginator()->getLimit();
        $this->queryParameters[':offset'] = (int)$this->getPaginator()->getOffset();

        $this->query->setFirstResult($this->queryParameters[':offset'])
            ->setMaxResults($this->queryParameters[':limit']);
    }
}
