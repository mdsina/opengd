<?php
namespace Core\InputFilter;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Core\Filter;
use Core\Validator;


class GridInputFilter extends InputFilter
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $inputParamFilter = new Input('filter');
        $inputParamFilter->setRequired(false);
        $inputParamFilter->getFilterChain()->attach(new Filter\Grid());

        $inputParamSort = new Input('sort');
        $inputParamSort->setRequired(false);
        $inputParamSort->getFilterChain()
            ->attach(new Filter\Grid());

        $this->add(Validator\PageNumber::get('page'));
        $this->add(Validator\ItemLimit::get('limit'));
        $this->add($inputParamFilter);
        $this->add($inputParamSort);
    }
}