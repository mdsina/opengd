<?php

namespace Core\Tools\Query\Filter;

use Core\Tools\Pagination\Paginator;
use \Doctrine\ORM\QueryBuilder as DQB;

class QueryBuilder
{
    /**
     * @param DQB $qb
     * @param Paginator $paginator
     * @param array $filtersMap
     *
     * @return DQB
     */
    public static function addConditions(DQB &$qb, Paginator $paginator, array $filtersMap = [])
    {
        foreach ($filtersMap as $parameter => $columnConfig) {
            if (!$paginator->hasSearchFilter($parameter)) {
                continue;
            }

            $filterValue = $paginator->getSearchFilter($parameter);

            if (is_callable($columnConfig)) {
                $q = call_user_func($columnConfig, $filterValue);

                if (!empty($q)) {
                    $qb->andWhere($q);
                }

                continue;
            }

            if ($filterValue == 'rating no set') {
                $filterValue = null;
            }

            if (is_null($filterValue)) {
                $qb->andWhere($qb->expr()->isNull($columnConfig['name']));

                continue;
            }

            switch ($columnConfig['type']) {
                case 'key':
                    $qb->andWhere($qb->expr()->eq($columnConfig['name'], $filterValue));

                    break;
                case 'string':
                    $pattern = !empty($columnConfig['pattern']) ? $columnConfig['pattern'] : '';
                    $pattern = str_replace(['%', 's'], ['%%', '%s'], $pattern);

                    if (empty($pattern)) {
                        $pattern = '%%%s%%';
                    }

                    $qb->andWhere($qb->expr()->like(
                        $qb->expr()->lower($columnConfig['name']),
                        $qb->expr()->lower(sprintf($pattern, $filterValue))
                    ));

                    break;
                case 'date':
                    $filterValue = str_replace('\\-', '-', $filterValue);

                    if (!preg_match('/^(([0-9]{4}-[0-9]{2}-[0-9]{2})|([0-9]{2}\.[0-9]{2}\.[0-9]{4}))$/', $filterValue)) {
                        continue;
                    }

                    $qb->andWhere($qb->expr()->like($columnConfig['name'], date('Y-m-d', strtotime($filterValue))));

                    break;
                case 'datetime':
                    $filterValue = str_replace('\\-', '-', $filterValue);

                    if (!preg_match('/^(([0-9]{4}-[0-9]{2}-[0-9]{2}\ [0-9]{2}:[0-9]{2}:[0-9]{2}))$/', $filterValue)) {
                        continue;
                    }

                    $qb->andWhere($qb->expr()->like($columnConfig['name'], $filterValue));

                    break;
                case 'float':
                    $qb->andWhere($qb->expr()->eq($columnConfig['name'], (float)$filterValue));

                    break;
                case 'int':
                case 'enum':
                default:
                    $qb->andWhere($qb->expr()->like(
                        sprintf('CAST(%s AS text)', $columnConfig['name']),
                        sprintf('%%%s%%', (int)$filterValue)
                    ));
            }
        }

        return $qb;
    }

    /**
     * @param DQB $qb
     * @param Paginator $paginator
     * @param array $sortMap
     * @return DQB
     * @throws \Exception
     */
    public static function addOrder(DQB &$qb, Paginator $paginator, array $sortMap)
    {
        $paginatorOrder = $paginator->getOrder();

        if ($paginatorOrder && isset($sortMap[$paginatorOrder->getName()]['name'])) {
            $orderField = $sortMap[$paginatorOrder->getName()]['name'];
            $orderDirect = $paginatorOrder->getDirectionWord();
            $qb->orderBy($orderField, $orderDirect);

            return $qb;
        }

        if ($paginatorOrder) {
            $attribute = $paginatorOrder->getName();
            $direction = $paginatorOrder->getDirectionWord() === 'DESC' ? SORT_DESC : SORT_ASC;
        } elseif (isset($sortMap['default'])) {
            $attribute = key($sortMap['default']);
            $direction = current($sortMap['default']);
        } else {
            return $qb;
        }

        if (!array_key_exists('attributes', $sortMap)) {
            throw new \InvalidArgumentException('Sorting map is invalid, it must contain "attribute" key');
        }

        if (!array_key_exists($attribute, $sortMap['attributes'])) {
            throw new \InvalidArgumentException("No such attribute \"$attribute\" in sorting map");
        }

        if (is_string($sortMap['attributes'][$attribute])) {
            $qb->orderBy($sortMap['attributes'][$attribute], $direction === SORT_DESC ? 'DESC' : 'ASC');

            return $qb;
        }

        if (is_array($sortMap['attributes'][$attribute])) {
            if (isset($sortMap['attributes'][$attribute][$direction])) {
                $qb->orderBy($sortMap['attributes'][$attribute][$direction], $direction);

                return $qb;
            }
        }

        throw new \InvalidArgumentException("Attribute config for attribute '$attribute' is invalid");
    }
}
