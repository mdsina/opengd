<?php

namespace Core\Tools\Query\Filter;

use Core\Tools\Pagination\Paginator;

class Sql
{
    /**
     * @TODO: Возможно стоит сделать фабрику для предикатов что бы генерировать платформонезависимый промежуточный код запроса для больше гибгости, с другой стороны это уменьшит нагрузку.
     *
     * @param string $sql
     * @param Paginator $paginator
     * @param array $filtersMap
     * @param bool $isFirst
     *
     * @return string
     */
    public static function addConditions($sql, Paginator $paginator, array $filtersMap = [], $isFirst = false)
    {
        foreach ($filtersMap as $parameter => $columnConfig) {
            if (!$paginator->hasSearchFilter($parameter)) {
                continue;
            }

            $filterValue = $paginator->getSearchFilter($parameter);

            if (is_callable($columnConfig)) {
                $q = call_user_func($columnConfig, $filterValue);

                if (!empty($q)) {
                    $sql .= sprintf(" %s (%s)\n", $isFirst ? 'WHERE' : 'AND', $q);
                    $isFirst = false;
                }

                continue;
            }

            if ($filterValue == 'rating no set') {
                $filterValue = null;
            }

            if (is_null($filterValue)) {
                $sql .= sprintf(" %s (%s IS NULL)\n", $isFirst ? 'WHERE' : 'AND', $columnConfig['name']);
                $isFirst = false;

                continue;
            }

            switch ($columnConfig['type']) {
                case 'key':
                    $sql .= sprintf(" %s %s='%s'\n", $isFirst ? 'WHERE' : 'AND', $columnConfig['name'], $filterValue);

                    break;
                case 'string':
                    $pattern = !empty($columnConfig['pattern']) ? $columnConfig['pattern'] : '';
                    $pattern = str_replace(['%', 's'], ['%%', '%s'], $pattern);

                    if (empty($pattern)) {
                        $pattern = '%%%s%%';
                    }

                    $sql .= sprintf(
                        " %s (LOWER(%s) LIKE LOWER('" . $pattern . "'))\n",
                        $isFirst ? 'WHERE' : 'AND',
                        $columnConfig['name'],
                        $filterValue
                    );

                    break;
                case 'date':
                    $filterValue = str_replace('\\-', '-', $filterValue);

                    if (!preg_match('/^(([0-9]{4}-[0-9]{2}-[0-9]{2})|([0-9]{2}\.[0-9]{2}\.[0-9]{4}))$/', $filterValue)) {
                        continue;
                    }

                    $sql .= sprintf(
                        " %s (%s::char(10) LIKE '%s')\n",
                        $isFirst ? 'WHERE' : 'AND',
                        $columnConfig['name'],
                        date('Y-m-d', strtotime($filterValue))
                    );

                    break;
                case 'datetime':
                    $filterValue = str_replace('\\-', '-', $filterValue);

                    if (!preg_match('/^(([0-9]{4}-[0-9]{2}-[0-9]{2}\ [0-9]{2}:[0-9]{2}:[0-9]{2}))$/', $filterValue)) {
                        continue;
                    }

                    $sql .= sprintf(
                        " %s (%s::char(19) LIKE '%s')\n",
                        $isFirst ? 'WHERE' : 'AND',
                        $columnConfig['name'],
                        $filterValue
                    );

                    break;
                case 'float':
                    $sql .= sprintf(
                        " %s (%s = '%s')\n",
                        $isFirst ? 'WHERE' : 'AND',
                        $columnConfig['name'],
                        (float)$filterValue
                    );

                    break;
                case 'int':
                case 'enum':
                default:
                    if ($parameter != 'house_id') {
                        $sql .= sprintf(
                            " %s (%s = '%s')\n",
                            $isFirst ? 'WHERE' : 'AND',
                            $columnConfig['name'],
                            (int)$filterValue
                        );
                    } else {
                        $sql .= sprintf(
                            " %s (CAST(%s AS text) LIKE '%%%s%%')\n",
                            $isFirst ? 'WHERE' : 'AND',
                            $columnConfig['name'],
                            (int)$filterValue
                        );
                    }
            }

            $isFirst = false;
        }

        return $sql;
    }

    /**
     * @param string $sql
     * @param Paginator $paginator
     * @param array $sortMap
     *
     * @throws \Exception
     * @return string
     */
    public static function addOrder($sql, Paginator $paginator, array $sortMap)
    {
        $paginatorOrder = $paginator->getOrder();

        if ($paginatorOrder && isset($sortMap[$paginatorOrder->getName()]['name'])) {
            $orderField = $sortMap[$paginatorOrder->getName()]['name'];
            $orderDirect = $paginatorOrder->getDirectionWord();
            $sql .= sprintf(" ORDER BY %s %s\n", $orderField, $orderDirect);

            return $sql;
        }

        if ($paginatorOrder) {
            $attribute = $paginatorOrder->getName();
            $direction = $paginatorOrder->getDirectionWord() === 'DESC' ? SORT_DESC : SORT_ASC;
        } elseif (isset($sortMap['default'])) {
            $attribute = key($sortMap['default']);
            $direction = current($sortMap['default']);
        } else {
            return $sql;
        }

        if (!array_key_exists('attributes', $sortMap)) {
            throw new \Exception('Sorting map is invalid, it must contain "attribute" key');
        }

        if (!array_key_exists($attribute, $sortMap['attributes'])) {
            throw new \Exception("No such attribute \"$attribute\" in sorting map");
        }

        if (is_string($sortMap['attributes'][$attribute])) {
            $sql .= sprintf(
                " ORDER BY %s %s\n",
                $sortMap['attributes'][$attribute],
                $direction === SORT_DESC ? 'DESC' : 'ASC'
            );

            return $sql;
        }

        if (is_array($sortMap['attributes'][$attribute])) {
            if (isset($sortMap['attributes'][$attribute][$direction])) {
                $sql .= sprintf(" ORDER BY %s\n", $sortMap['attributes'][$attribute][$direction]);

                return $sql;
            }
        }

        throw new \Exception("Attribute config for attribute '$attribute' is invalid");
    }
}
