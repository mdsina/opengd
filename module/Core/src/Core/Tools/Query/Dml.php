<?php


namespace Core\Tools\Query;


use Zend\Filter\Word\UnderscoreToCamelCase;

class Dml {
    /**
     * Преобразует ключи массива в camelCase
     *
     * @param array $data
     *
     * @return array
     */
    public static function camelizeKeys($data)
    {
        if (!is_array($data)) {
            return $data;
        }

        $out = [];
        $filter = new UnderscoreToCamelCase();

        foreach ($data as $key => $item) {
            $out[str_replace('_', '', lcfirst($filter->filter($key)))] = is_scalar($item)
                ? $item
                : self::camelizeKeys($item);
        }

        return $out;
    }
}