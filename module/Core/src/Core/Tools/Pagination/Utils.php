<?php

namespace Core\Tools\Pagination;

class Utils
{
    const DATETIME_FORMAT = 'd.m.Y H:i:s';
    const DATE_FORMAT = 'd.m.Y';

    /**
     * @param \DateTime $datetime
     * @return string
     */
    public static function formatDatetime($datetime)
    {
        if (is_object($datetime)) {
            return $datetime->format(self::DATETIME_FORMAT);
        }

        return '';
    }

    /**
     * @param \DateTime $date
     * @return string
     */
    public static function formatDate($date)
    {
        if (is_object($date)) {
            return $date->format(self::DATE_FORMAT);
        }

        return '';
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * Используется когда надо посчитать время выполнения функции, куска кода
     * @param $func
     * @return array [0 => <время выполнения>, 1 => <результаты фунции>];
     * @throws \InvalidArgumentException
     */
    public static function timeMeasurable($func)
    {
        if (!is_callable($func)) {
            throw new \InvalidArgumentException('Expected argument of type "callable", got "' . gettype($func) . '"');
        }

        $time = microtime(true);
        $result = call_user_func($func);
        $runTime = sprintf("%.6f", microtime(true) - $time);

        return [$runTime, $result ];
    }

    /**
     * @param $arr
     * @param int $c
     * @return mixed
     * @link http://php.net/manual/ru/function.array-change-key-case.php#107715 see
     */
    public static function array_change_key_case_unicode($arr, $c = CASE_LOWER) 
    {
        if (!is_array($arr)) {
            throw new \InvalidArgumentException('$arr parameter must be an array');
        }

        $c = ($c == CASE_LOWER) ? MB_CASE_LOWER : MB_CASE_UPPER;
        $ret = [];

        foreach ($arr as $k => $v) {
            $ret[mb_convert_case($k, $c, "UTF-8")] = $v;
        }

        return $ret;
    }
}
