<?php

namespace Core\Tools\Pagination;

use Zend\InputFilter\InputFilter;


class Paginator
{
    const DEFAULT_PAGE_NUMBER = 1;
    const DEFAULT_ELEMENTS_PER_PAGE = 20;

    /**
     * Номер страницы
     *
     * @var int
     */
    protected $pageNumber = self::DEFAULT_PAGE_NUMBER;

    /**
     * Количество записей на странице
     *
     * @var int
     */
    protected $elementsPerPage = self::DEFAULT_ELEMENTS_PER_PAGE;

    /**
     * Сортировки
     *
     * @var PaginatorOrder[]
     */
    protected $orders = [];

    /**
     * Общее количество элементов по результатам поиска
     *
     * @var int
     */
    protected $elementCount;

    /**
     * Пользовательские фильтры
     *
     * @var array
     */
    protected $userFilters;

    protected $filters;


    /**
     * @param int $pageNumber
     * @param int $elementsPerPage
     * @param string $firstOrderName
     * @param bool $firstOrderReverse
     * @param bool $elementCount
     * @param array $userFilters
     * @param array $searchFilters
     */
    public function __construct(
        $pageNumber = 1,
        $elementsPerPage = 20,
        $firstOrderName = '',
        $firstOrderReverse = false,
        $elementCount = false,
        array $userFilters = [],
        array $searchFilters = []
    ) {
        $this->setCurrentPageNumber($pageNumber);
        $this->setElementsPerPage($elementsPerPage);
        $this->elementCount = $elementCount;
        $this->userFilters = $userFilters;
        $this->filters = $searchFilters;

        if ($firstOrderName) {
            $this->orders = [new PaginatorOrder($firstOrderName, $firstOrderReverse)];
        }
    }

    /**
     * Считается хорошим тоном использовать InputFilter для проверки данных от пользователя
     * Ниже, стандартные проверки для ais - использовать этот метод не обязательно
     *
     * @param InputFilter $inputFilter
     */
    public function prepare(InputFilter $inputFilter)
    {
        $this->setPageNumber($inputFilter->getValue('page'));
        $this->setElementsPerPage($inputFilter->getValue('limit'));

        if ($inputFilter->getValue('filter')) {
            $this->prepareFilters($inputFilter->getValue('filter'));
        }

        if ($inputFilter->getValue('sort')) {
            $this->prepareOrders($inputFilter->getValue('sort'));
        }
    }

    /**
     * Настройка сортировки
     *
     * @param $sorts array
     */
    public function prepareOrders($sorts)
    {
        if (is_array($sorts)) {
            $sorts = array_values($sorts);
            foreach ($sorts as $i => $sort) {
                if (is_object($sort)
                    && property_exists($sort, 'property')
                    && property_exists($sort, 'direction')
                ) {
                    $name = $sort->property;
                    $isReverse = ($sort->direction == 'ASC') ? false : true;
                    $order = new PaginatorOrder($name, $isReverse);
                    $this->setOrder($order, $i);
                }
            }
        }
    }

    /**
     * Настройка фильтров пагинатора
     *
     * @param $filters array
     */
    public function prepareFilters($filters)
    {
        if (is_array($filters)) {
            foreach ($filters as $filter) {
                if (is_object($filter) && property_exists($filter, 'property')) {
                    $value = property_exists($filter, 'value') ? $filter->value : null;
                    $this->setSearchFilter($filter->property, $value);
                }
            }
        }
    }

    /**
     * Метод для статического создания пагинатора из объекта запроса
     *
     * @param array|InputFilter $query
     * @return Paginator
     */
    public static function newFromQuery($query)
    {
        if ($query instanceof InputFilter) {
            $query = $query->getValues();
        }

        $p = new Paginator();
        foreach ($query as $k => $q) {
            switch ($k) {
                case 'page':
                    $p->setCurrentPageNumber($q);

                    break;
                case 'perpage':
                case 'limit':
                    $p->setElementsPerPage($q);

                    break;
                case 'sort':
                    $json = json_decode($q, true);

                    if (json_last_error() == JSON_ERROR_NONE && is_array($json)) {
                        foreach ($json as $i => $v) {
                            if (isset($json[$i]['property'])) {
                                $p->setOrder(new PaginatorOrder(
                                    $json[$i]['property'],
                                    (isset($json[$i]['direction']) && $json[$i]['direction'] == 'DESC')
                                ), $i);
                            }
                        }
                    } else {
                        $p->setOrder(
                            new PaginatorOrder(
                                $q,
                                (isset($query['inverted']) && $query['inverted'])
                                || (isset($query['order']) && $query['order'] == 'desc')
                            )
                        );
                    }

                    break;
                case 'filter':
                    $json = json_decode($q, true);

                    if (json_last_error() == JSON_ERROR_NONE && is_array($json)) {
                        foreach ($json as $v) {
                            if (isset($v['property']) && isset($v['value'])) {
                                $p->setSearchFilter($v['property'], $v['value']);
                            }
                        }
                    } elseif (isset($query['search'])) {
                        $p->setSearchFilter($q, $query['search']);
                    }

                    break;
                default:
                    $p->setUserFilter($k, $q);

                    break;
            }
        }

        return $p;
    }

    /**
     * @return Paginator
     */
    public static function newEmpty()
    {
        return new Paginator(0, 0);
    }

    /**
     * Возвращает количество страниц
     *
     * @return float|int
     */
    public function getPageCount()
    {
        if ($this->getElementCount()) {
            return ceil($this->getElementCount() / $this->getElementsPerPage());
        }

        return $this->getElementCount();
    }

    /**
     * Задает объект для сортировка
     *
     * @param PaginatorOrder $order
     * @param int $whichOrder
     */
    public function setOrder(PaginatorOrder $order, $whichOrder = 0)
    {
        $this->orders[$whichOrder] = $order;
    }

    /**
     * Возвращает сортировку (по индексу)
     *
     * @param int $whichOrder
     *
     * @return PaginatorOrder|null
     */
    public function getOrder($whichOrder = 0)
    {
        return array_key_exists($whichOrder, $this->orders) ? $this->orders[$whichOrder] : null;
    }

    /**
     * @return int
     */
    public function getOrderCount()
    {
        return count($this->orders);
    }

    /**
     * Фильтр для SELECT
     * @param $name
     * @param $value
     * @internal param mixed $filters
     */
    public function setSearchFilter($name, $value)
    {
        $this->filters[$name] = $value;
    }

    /**
     * Фильтр для SELECT
     * @param $name
     * @return string
     */
    public function getSearchFilter($name)
    {
        if (!isset($this->filters[$name])) {
            return null;
        }

        $value = $this->filters[$name];

        return $value;
    }

    /**
     * @return int
     */
    public function getSearchFilterCount()
    {
        return count($this->filters);
    }

    /**
     * @return array
     */
    public function getSearchFilterArray()
    {
        return $this->filters;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasSearchFilter($name)
    {
        return array_key_exists($name, $this->filters);
    }

    /**
     * Custom advanced data filter, for custom specs only!
     * @param $name
     * @return null
     */
    public function getUserFilter($name)
    {
        return (isset($this->userFilters[$name])) ? $this->userFilters[$name] : null;
    }

    /**
     * Custom advanced data filter, for custom specs only!
     * @param $name
     * @param $value
     */
    public function setUserFilter($name, $value)
    {
        $this->userFilters[$name] = $value;
    }

    /**
     * Custom advanced data filter, for custom specs only!
     * @return array
     */
    public function getUserFilterArray()
    {
        return $this->userFilters;
    }

    /**
     * Возвращает кол-во элементов на страницу
     *
     * @return int
     */
    public function getLimit()
    {
        return $this->getElementsPerPage();
    }

    /**
     * Возвращает смещение
     *
     * @return int
     */
    public function getOffset()
    {
        return ($this->getCurrentPageNumber() - 1) * $this->getElementsPerPage();
    }

    /**
     * @return int
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     */
    public function setPageNumber($pageNumber)
    {
        $this->pageNumber = $this->cleanInt($pageNumber, self::DEFAULT_PAGE_NUMBER);
    }

    /**
     * @return int
     */
    public function getCurrentPageNumber()
    {
        return $this->getPageNumber();
    }

    /**
     * @param int $pageNumber
     */
    public function setCurrentPageNumber($pageNumber)
    {
        $this->setPageNumber($pageNumber);
    }

    /**
     * @return array
     */
    public function getUserFilters()
    {
        return $this->userFilters;
    }

    /**
     * @param array $userFilters
     */
    public function setUserFilters($userFilters)
    {
        $this->userFilters = $userFilters;
    }

    /**
     * @return int
     */
    public function getElementsPerPage()
    {
        return $this->elementsPerPage;
    }

    /**
     * @param int $elementsPerPage
     */
    public function setElementsPerPage($elementsPerPage)
    {
        $this->elementsPerPage = $this->cleanInt($elementsPerPage, self::DEFAULT_ELEMENTS_PER_PAGE);
    }

    /**
     * @return int
     */
    public function getElementCount()
    {
        return $this->elementCount;
    }

    /**
     * @param int $elementCount
     */
    public function setElementCount($elementCount)
    {
        $this->elementCount = $elementCount;
    }

    public function isEmpty()
    {
        return $this->pageNumber == 0 && $this->elementsPerPage == 0;
    }

    /**
     * @param mixed $value
     * @param int $default
     * @return int
     */
    protected function cleanInt($value, $default)
    {
        $value = filter_var($value, FILTER_VALIDATE_INT);

        if (!$value || $value <= 0) {
            return $default;
        }

        return $value;
    }
}
