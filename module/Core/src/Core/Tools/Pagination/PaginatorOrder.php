<?php

namespace Core\Tools\Pagination;

class PaginatorOrder
{
    /**
     * @var string название
     */
    protected $name;

    /**
     * @var bool порядок сортировки
     */
    protected $isReverse;

    /**
     * @var array
     */
    protected $fieldOrder = [];

    /**
     * @param string $name название поля для сортировки
     * @param bool $isReverse порядок сортировки
     * @param array $fieldOrder массив значений поля $name для сортировки BY FIELD(name, val1, val2...valN)
     */
    public function __construct($name, $isReverse = false, $fieldOrder = [])
    {
        $this->name = $name;
        $this->fieldOrder = $fieldOrder;

        if (is_bool($isReverse)) {
            $this->isReverse = $isReverse;
        } else {
            $this->isReverse = (strtoupper($isReverse) == 'DESC');
        }
    }

    public function getDirectionWord()
    {
        if (!empty($this->fieldOrder)) {
            return '';
        }

        return $this->isReverse ? 'DESC' : 'ASC';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isReverse()
    {
        return $this->isReverse;
    }

    /**
     * @param boolean $isReverse
     */
    public function setIsReverse($isReverse)
    {
        $this->isReverse = $isReverse;
    }

    /**
     * @return array
     */
    public function getFieldOrder()
    {
        return $this->fieldOrder;
    }

    /**
     * @param array $fieldOrder
     */
    public function setFieldOrder($fieldOrder)
    {
        $this->fieldOrder = $fieldOrder;
    }
}
