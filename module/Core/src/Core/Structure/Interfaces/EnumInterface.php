<?php

namespace Core\Structure\Interfaces;


interface EnumInterface
{
    public static function getList();
}