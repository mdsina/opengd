<?php

namespace Core\Structure;

use Core\Structure\Interfaces\EnumInterface;

abstract class AbstractEnum implements EnumInterface
{
    private static $cache = [];

    public static function getList()
    {
        if (!isset(self::$cache[get_called_class()])) {
            $reflect = new \ReflectionClass(get_called_class());
            self::$cache[get_called_class()] = $reflect->getConstants();
        }

        return self::$cache[get_called_class()];
    }
}