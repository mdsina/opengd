<?php


namespace Core\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class BaseAppController
 * @package Core\Controller
 *
 * @method \Zend\Http\Request getRequest()
 * @method \Core\Controller\Plugin\PrepareFormPlugin PrepareFormPlugin()
 * @method \Auth\Entity\User|null identity()
 */
class BaseAppController extends AbstractActionController
{
    /**
     * @param string $message
     * @return string
     */
    protected function translate($message)
    {
        return $this->getService('translator')->translate($message);
    }

    /**
     * Retrieve a registered instance
     *
     * @param  string $name
     * @param array $params
     * @return array|object
     */
    protected function getService($name, $params = [])
    {
        return $this->getServiceLocator()->get($name, $params);
    }

    /**
     * @param string $name
     * @return mixed|callable
     */
    public function getViewHelper($name)
    {
        return $this->getService('ViewHelperManager')->get($name);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getForm($name)
    {
        return $this->getService('FormElementManager')->get($name);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->getService('Config');
    }
}