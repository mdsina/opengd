<?php

namespace Core\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class PrepareFormPlugin extends AbstractPlugin
{
    /**
     * Преобразует массив ошибок формы в понятный для ExtJs массив ошибок
     *
     * @param array $messages Массив ошибок формы
     * @param string $prefix Префикс, используется для генерации названия поля
     * @return array
     */
    public function getErrorValidate($messages, $prefix = '')
    {
        $errorValidate = [];

        if ($this->isFieldset($messages)) {
            foreach ($messages as $id => $fieldset) {
                $errorValidate = array_merge(
                    $errorValidate,
                    $this->getErrorValidate($fieldset, $prefix ? sprintf('%s[%s]', $prefix, $id) : $id)
                );
            }

            return $errorValidate;
        }

        $errorValidate[] = $this->convertErrorMessages($prefix, $messages);

        return $errorValidate;
    }

    public function __invoke($messages = null)
    {
        if ($messages) {
            return $this->getErrorValidate($messages);
        }

        return $this;
    }

    private function isFieldset($messages)
    {
        foreach ($messages as $message) {
            if (!is_array($message)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Формирует массив ошибок для поля
     *
     * @param string $field Название поля
     * @param array $messages Массив ошибок
     * @return array
     */
    private function convertErrorMessages($field, array $messages)
    {
        $errors = [];
        foreach ($messages as $validator => $error) {
            $errors[] = $error;
        }

        return ['id' => $field, 'msg' => $errors];
    }
}
