<?php


namespace Core\Controller;

use Zend\View\Model\JsonModel;

/**
 * Class JsonViewController
 * @package Core\Controller
 */
class JsonViewController extends BaseAppController
{
    /**
     * @param array $json
     * @return JsonModel
     */
    protected function json(array $json = [])
    {
        return new JsonModel($json);
    }

    /**
     * @param string $message
     * @return JsonModel
     */
    protected function jsonSuccess($message = '')
    {
        return new JsonModel([
            'success' => true,
            'msg' => $message,
        ]);
    }

    /**
     * @param string $message
     * @return JsonModel
     */
    protected function jsonFailure($message = '')
    {
        return new JsonModel([
            'success' => false,
            'msg' => $message,
        ]);
    }

    /**
     * @param array $data
     * @return JsonModel
     */
    protected function jsonData(array $data = [])
    {
        return new JsonModel([
            'success' => true,
            'data' => $data,
        ]);
    }
}