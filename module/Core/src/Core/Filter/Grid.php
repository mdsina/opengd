<?php

namespace Core\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Json\Json;

class Grid extends AbstractFilter
{
    public function filter($value)
    {
        return Json::decode($value);
    }
}
