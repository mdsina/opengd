<?php


namespace Admin\Controller;

use Auth\Entity\User;
use Auth\Repository\UserRepository;
use Core\Controller\JsonViewController;
use Core\Grid\BasePaginatorGrid;
use Doctrine\ORM\EntityManager;

class UserController extends JsonViewController
{
    /**
     * @return \Doctrine\ORM\EntityRepository|UserRepository
     */
    private function getUserRepository()
    {
        /** @var EntityManager $em */
        $em = $this->getService('Doctrine\ORM\EntityManager');

        return $em->getRepository(User::class);
    }

    public function listAction()
    {
        /** @var BasePaginatorGrid $bpg */
        $bpg = $this->getService('Core\Grid\BasePaginatorGrid');

        if (!$bpg->prepare('Admin\Grid\UsersListGrid', $this->params()->fromQuery())) {
            return $this->jsonFailure($bpg->getPrepareErrors());
        }

        return $this->json($bpg->getList());
    }
}