<?php

namespace Admin\Controller;

use Core\Controller\JsonViewController;
use Core\Grid\BasePaginatorGrid;

class RoleController extends JsonViewController
{
    public function listAction()
    {
        /** @var BasePaginatorGrid $bpg */
        $bpg = $this->getService('Core\Grid\BasePaginatorGrid');

        if (!$bpg->prepare('Admin\Grid\RolesListGrid', $this->params()->fromQuery())) {
            return $this->jsonFailure($bpg->getPrepareErrors());
        }

        return $this->json($bpg->getList());
    }
}