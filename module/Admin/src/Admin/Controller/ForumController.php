<?php


namespace Admin\Controller;


use Admin\Form\ForumCreate;
use Core\Controller\JsonViewController;
use Doctrine\ORM\EntityManager;
use Forum\Entity\ForumCategory;
use Forum\Repository\ForumRepository;

class ForumController extends JsonViewController
{
    /**
     * @return \Doctrine\ORM\EntityRepository|ForumRepository
     */
    private function getForumRepository()
    {
        /** @var EntityManager $em */
        $em = $this->getService('Doctrine\ORM\EntityManager');

        return $em->getRepository(ForumCategory::class);
    }

    public function createAction()
    {
        /** @var ForumCreate $form */
        $form = $this->getForm('ForumCreateForm');

        if (!$this->getRequest()->isPost()) {
            return $this->jsonFailure($this->translate('Only POST request allowed'));
        }

        $data = $this->params()->fromPost();

        $form->setData($data);

        if (!$form->isValid()) {
            return $this->json([
                'success' => false,
                'errorValidate' => $this->PrepareFormPlugin()->getErrorValidate($form->getMessages()),
            ]);
        }

        $forumRep = $this->getForumRepository();

        /** @var \Forum\Form\Fieldset\ForumCategory $fieldset */
        $fieldset = $form->getFieldsets()[ForumCreate::ELEMENT_MAIN_FIELDSET];
        $forumRep->createCategory($fieldset->getObject());

        return $this->jsonSuccess($this->translate('Record was successfully added'));
    }

    public function removeAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->jsonFailure($this->translate('Only POST request allowed'));
        }

        /** @var ForumCategory  $forum */
        $forumRep = $this->getForumRepository();
        $forum = $forumRep->findOneBy(['id' => $this->params()->fromPost('id')]);

        if (!$forum) {
            return $this->jsonFailure($this->translate('Object not found'));
        }

        $forumRep->deleteCategory($forum);

        return $this->jsonSuccess();
    }

    public function listAction()
    {
        $forumRep = $this->getForumRepository();
        $arr = $forumRep->fetchRoots();
        $helper = $this->getViewHelper('forumtoarray');

        return $this->json(['text' => '.', 'children' => $helper($arr)]);
    }
}