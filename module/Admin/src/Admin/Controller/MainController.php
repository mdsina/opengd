<?php


namespace Admin\Controller;

use Core\Controller\JsonViewController;

class MainController extends JsonViewController
{
    public function getNavigationAction()
    {
        $navigation = $this->getConfig()['navigation'];

        return $this->json($navigation);
    }
}