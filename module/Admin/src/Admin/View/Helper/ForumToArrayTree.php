<?php


namespace Admin\View\Helper;


use Forum\Entity\ForumCategory;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ForumToArrayTree
 * @package Admin\View\Helper
 */
class ForumToArrayTree extends AbstractHelper
{
    /**
     * @param ForumCategory[] $forums
     * @return array
     */
    public function __invoke($forums)
    {
        $res = [];
        foreach ($forums as $forum) {
            $item = [
                'id' => $forum->getId(),
                'title' => $forum->getTitle(),
                'name' => $forum->getName(),
                'description' => $forum->getDescription(),
                'parent' => $forum->getParent() ? $forum->getParent()->getId() : null,
                'disabled' => $forum->isDisabled(),
                'manuallyCreated' => $forum->isManuallyCreated(),
            ];

            if ($forum->getOwner()) {
                $item['owner'] = [
                    'id' => $forum->getOwner()->getId(),
                    'username' => $forum->getOwner()->getUsername(),
                ];
            }

            if ($forum->hasChildren()) {
                $item['expanded'] = true;
                $item['children'] = $this->__invoke($forum->getChildren());
            } else {
                $item['leaf'] = true;
            }

            $res[] = $item;
        }

        return $res;
    }
}