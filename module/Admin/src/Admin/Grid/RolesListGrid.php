<?php

namespace Admin\Grid;



use Core\Grid\EntityGrid;

class RolesListGrid extends EntityGrid
{
    protected $filtersMap = [
        'id'         => ['name' => 'r.id', 'type' => 'int'],
        'name'       => ['name' => 'r.name', 'type' => 'key'],
        'type'       => ['name' => 'r.type', 'type' => 'int'],
    ];

    protected function prepareQuery()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('r.id, r.name, r.type')
            ->from('Auth\Entity\Role', 'r');

        $this->setQuery($qb);

        parent::prepareQuery();
    }


}
