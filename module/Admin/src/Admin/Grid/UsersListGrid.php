<?php

namespace Admin\Grid;



use Core\Grid\EntityGrid;

class UsersListGrid extends EntityGrid
{
    protected $filtersMap = [
        'id'         => ['name' => 'u.id', 'type' => 'int'],
        'email'      => ['name' => 'u.email', 'type' => 'key'],
        'username'   => ['name' => 'u.username', 'type' => 'key'],
    ];

    protected function prepareQuery()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u.id, u.email, u.username')
            ->from('Auth\Entity\User', 'u');

        $this->setQuery($qb);

        parent::prepareQuery();
    }


}
