<?php


namespace Admin\Form;

use DoctrineExt\Mapper\EntityManagerAwareInterface;
use DoctrineExt\Mapper\EntityManagerAwareTrait;
use DoctrineExt\Validator\ObjectExists;
use Forum\Entity\ForumCategory;
use Forum\Repository\ForumRepository;
use Zend\Form\Element\Select;
use Zend\Form\FieldsetInterface;
use Zend\Form\Form;


class ForumCreate extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /** элементы формы */
    const ELEMENT_MAIN_FIELDSET = 'fsOne';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->add([
            'name' => self::ELEMENT_MAIN_FIELDSET,
            'type' => 'Forum\Form\Fieldset\ForumCategory',
            'options' => [
                'label' => 'Create forum',
                'use_as_base_fieldset' => true,
            ]
        ]);
    }
}
