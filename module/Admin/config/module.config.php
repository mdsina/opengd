<?php

namespace Admin;

$router = include(__DIR__ . '/router.config.php');
$permissions = include(__DIR__ . '/permissions.config.php');
$navigation = include(__DIR__ . '/navigation.config.php');

return array_merge($router, $permissions, $navigation, [
    'translator' => [
        'locale' => 'ru',
        'translation_file_patterns' => [
            [
                'type' => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => 'lang.array.%s.php',
            ],
        ],
    ],

    'form_elements' => [
        'invokables' => [
            'ForumCreateForm' => 'Admin\Form\ForumCreate',
        ],
    ],

    'service_manager' => [
        'factories' => [
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ],
        'invokables' => [
            'Admin\Grid\UsersListGrid' => 'Admin\Grid\UsersListGrid',
            'Admin\Grid\RolesListGrid' => 'Admin\Grid\RolesListGrid',
        ],
    ],

    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],

    'view_helpers' => [
        'invokables' => [
            'forumtoarray' => 'Admin\View\Helper\ForumToArrayTree',
        ],
    ],
]);
