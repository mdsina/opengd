<?php


return [
    'navigation' => [
        [
            'text' => 'Стартовая страница',
            'href' => '#',
            'leaf' => true,
        ],
        [
            'text' => 'Список форумов',
            'href' => '#forums',
            'leaf' => true,
        ],
        [
            'text' => 'Пользователи',
            'expanded' => true,
            'children' => [
                [
                    'text' => 'Список пользователей',
                    'href' => '#users',
                    'leaf' => true,
                ],
            ],
        ],
        [
            'text' => 'Администрирование',
            'expanded' => true,
            'children' => [
                [
                    'text' => 'Список ролей',
                    'href' => '#administrate/roles',
                    'leaf' => true,
                ],
            ],
        ],
    ],
];