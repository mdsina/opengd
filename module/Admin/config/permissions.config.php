<?php

namespace Admin;

use Auth\Enum\Permissions;

return [
    'zfc_rbac' => [
        'guards' => [
            'ZfcRbacAdditions\Guard\ControllerAndActionPermissionsGuard' => [
                'Admin\Controller\Forum' => [
                    ['action' => 'create', 'permissions' => [Permissions::ADMIN_FORUMS_ADD]],
                ],
                'Admin\Controller\Main' => [
                    ['action' => 'get-navigation', 'permissions' => [Permissions::ADMIN_NAVIGATION_VIEW]],
                ],
            ],
        ]
    ],
];
