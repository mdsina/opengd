<?php

namespace Admin;

return [
    'controllers' => [
        'invokables' => [
            'Admin\Controller\Forum' => 'Admin\Controller\ForumController',
            'Admin\Controller\User' => 'Admin\Controller\UserController',
            'Admin\Controller\Main' => 'Admin\Controller\MainController',
            'Admin\Controller\Role' => 'Admin\Controller\RoleController',
        ],
    ],

    'router' => [
        'routes' => [
            'admin' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/admin-backend',
                    'defaults' => [
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Main',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                            ],
                        ],
                    ],
                ],
            ],
        ]
    ],
];
