<?php

return [
    'Create forum' => 'Создать форум',

    'Name' => 'Имя',
    'Title' => 'Заголовок',
    'Description' => 'Описание',

    'Unique name likes "some_unique_name". If empty will be generated from Title' => 'Уникальное имя "some_unique_name". Если не заполнено, генерируется автоматически из Заголовка',
    'The title of forum' => 'Заголовок форума',
    'Description of forum' => 'Описание форума',
    'Parent forum' => 'Основной форум',

    'Submit' => 'Отправить',

    'Value is required and can\'t be empty' => 'Поле обязательно для заполнения и не может быть пустым',
    'Invalid input!' => 'Ошибка!',
    'The form fields are not filled out correctly.' => 'Некоторые поля формы были заполненны некорректно.',

    'An object matching \'%value%\' was found' => 'Запись с атрибутом \'%value%\' уже существует',

    'The input is more than %max% characters long' => 'Максимальная длина строки %max% символов',
    'Only POST request allowed' => 'Разрешены только POST-запросы',
    'Object not found' => 'Запись не найдена',

    'Record was successfully added' => 'Запись успешно добавлена',
];
