<?php
namespace ZfcRbacAdditions;

use ZfcRbac\Guard\GuardInterface;
use ZfcRbacAdditions\Factory\ControllerAndActionPermissionsGuardFactory;
use ZfcRbacAdditions\Guard\ControllerAndActionPermissionsGuard;

return [
    'zfc_rbac' => [
        'protection_policy' => GuardInterface::POLICY_ALLOW, //разрешить всё что не запрещено, т.е. если правила нет, то разрешаем
        'guard_manager' => [
            'factories' => [
                ControllerAndActionPermissionsGuard::class => ControllerAndActionPermissionsGuardFactory::class,
            ]
        ],
        'guards' => [
            ControllerAndActionPermissionsGuard::class => [],
        ],
    ]
];
