<?php

namespace ZfcRbacAdditions\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ZfcRbac\Service\AuthorizationService;
use ZfcRbacAdditions\Guard\ControllerAndActionPermissionsGuard;

/**
 * Class ControllerAndActionPermissionsGuardFactory
 * @package ZfcRbacAdditions\Factory
 */
class ControllerAndActionPermissionsGuardFactory implements FactoryInterface
{
    /**
     * @param \Zend\ServiceManager\AbstractPluginManager|ContainerInterface $container
     * @param string $requestedName
     * @param array $options
     * @return ControllerAndActionPermissionsGuard
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /* @var \ZfcRbac\Options\ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('ZfcRbac\Options\ModuleOptions');

        /* @var AuthorizationService $authorizationService */
        $authorizationService = $container->get('ZfcRbac\Service\AuthorizationService');

        $routeGuard = new ControllerAndActionPermissionsGuard($authorizationService, $container, $options);
        $routeGuard->setProtectionPolicy($moduleOptions->getProtectionPolicy());

        return $routeGuard;
    }
}
