<?php

namespace ZfcRbacAdditions\Guard;

use Zend\Http;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcRbac\Exception\RoleNotFoundException;
use ZfcRbac\Service\AuthorizationServiceInterface;
use ZfcRbac\Guard\ControllerPermissionsGuard;

/**
 * Class ControllerAndActionPermissionsGuard
 * @package ZfcRbacAdditions\Guard
 */
class ControllerAndActionPermissionsGuard extends ControllerPermissionsGuard
{
    const EVENT_PRIORITY = 1;

    /**
     * @param AuthorizationServiceInterface $authorizationService
     * @param ServiceLocatorInterface $serviceLocator
     * @param array $rules
     */
    public function __construct(
        AuthorizationServiceInterface $authorizationService,
        ServiceLocatorInterface $serviceLocator,
        array $rules = []
    ) {
        $this->serviceLocator = $serviceLocator;
        parent::__construct($authorizationService, $rules);
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules)
    {
        $this->rules = [];

        foreach ($rules as $controller => $rule) {
            if (!isset($this->rules[$controller])) {
                $this->rules[$controller] = [];
            }

            foreach ($rule as $params) {
                $methods = empty($params['methods'])
                    ? []
                    : (is_array($params['methods'])
                        ? $params['methods']
                        : [$params['methods']]);

                $this->rules[$controller][] = [
                    'action' => $params['action'],
                    'methods' => $methods,
                    'permissions' => $params['permissions'],
                ];
            }
        }
    }

    /**
     * @param MvcEvent $event
     * @return bool
     */
    public function isGranted(MvcEvent $event)
    {
        $action = $event->getRouteMatch()->getParam('action');
        $action = $this->normalizeActionName($action);

        $namespace = $event->getRouteMatch()->getParam('__NAMESPACE__');
        $controller = $event->getRouteMatch()->getParam('controller');

        if (!empty($namespace)) {
            if (0 !== strpos($controller, $namespace)) {
                $controller = $namespace .'\\' . ucfirst($controller);
            }
        }

        // If no rules apply, it is considered as granted or not based on the protection policy
        if (!isset($this->rules[$controller])) {
            return $this->protectionPolicy === self::POLICY_ALLOW;
        }

        $allowedPermissions = [];

        foreach ($this->rules[$controller] as $rule) {
            if ($this->normalizeActionName($rule['action']) != $action) {
                continue;
            }

            $allowedPermissions = $rule['permissions'];
        }

        // If no rules apply, it is considered as granted or not based on the protection policy
        if (empty($allowedPermissions)) {
            return $this->protectionPolicy === self::POLICY_ALLOW;
        }

        if (in_array('*', $allowedPermissions)) {
            return true;
        }

        foreach ($allowedPermissions as $permission) {
            try {
                if (!$this->authorizationService->isGranted($permission)) {
                    return false;
                }
            } catch (RoleNotFoundException $e) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $actionName
     * @return string
     */
    private function normalizeActionName($actionName)
    {
        $normalized = str_replace(['.', '-', '_'], '', $actionName);
        $normalized = strtolower($normalized);

        return $normalized;
    }
}
