<?php

namespace Migrations;

use Auth\Entity\Role;
use Auth\Enum\Permissions;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151006213459_create_roles extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $guestRole = $this->registerGuestRole();
        $newbieRole = $this->registerNewbieRole();
        $memberRole = $this->registerMemberRole();
        $adminRole = $this->registerAdminRole();

        $guestRole->addChild($newbieRole);
        $newbieRole->addChild($memberRole);
        $memberRole->addChild($adminRole);

        $this->getEntityManager()->persist($guestRole);

        $this->getEntityManager()->flush();
    }

    private function registerGuestRole()
    {
        $guestRole = new Role();
        $guestRole->setName('guest')
            ->setDisplayName('Гость')
            ->setType(Role::TYPE_SYSTEM);

        return $guestRole;
    }

    private function registerNewbieRole()
    {
        $newbieRole = new Role();
        $newbieRole->setName('newbie')
            ->setDisplayName('Новичок')
            ->setType(Role::TYPE_SYSTEM);

        $newbieRole->addPermission(Permissions::FORUMS_ADD_TO_FAV)
            ->addPermission(Permissions::FORUMS_SUBSCRIBE)
            ->addPermission(Permissions::TOPICS_OWN_CLOSE)
            ->addPermission(Permissions::TOPICS_OWN_EDIT)
            ->addPermission(Permissions::TOPICS_OWN_REMOVE)
            ->addPermission(Permissions::TOPICS_CREATE)
            ->addPermission(Permissions::MESSAGES_ADD_TO_FAV)
            ->addPermission(Permissions::MESSAGES_CREATE)
            ->addPermission(Permissions::MESSAGES_OWN_EDIT)
            ->addPermission(Permissions::MESSAGES_OWN_REMOVE)
            ->addPermission(Permissions::USERS_MANAGE_FRIENDS)
            ->addPermission(Permissions::USERS_MANAGE_BLOCKED)
            ->addPermission(Permissions::USERS_SEARCH)
            ->addPermission(Permissions::USERS_SEND_MESSAGE)
            ->addPermission(Permissions::USERS_PROFILE_VIEW)
            ->addPermission(Permissions::USERS_PROFILE_EDIT);

        return $newbieRole;
    }

    private function registerMemberRole()
    {
        $memberRole = new Role();
        $memberRole->setName('member')
            ->setDisplayName('Участник')
            ->setType(Role::TYPE_SYSTEM);

        $memberRole->addPermission(Permissions::TOPICS_OWN_MANAGE_IMPORTANCE);

        return $memberRole;
    }

    private function registerAdminRole()
    {
        $adminRole = new Role();
        $adminRole->setName('admin')
            ->setDisplayName('Администратор')
            ->setType(Role::TYPE_SYSTEM);

        $adminRole->addPermission(Permissions::ADMIN_FORUMS_ADD)
            ->addPermission(Permissions::ADMIN_FORUMS_EDIT)
            ->addPermission(Permissions::ADMIN_FORUMS_REMOVE)
            ->addPermission(Permissions::ADMIN_FORUMS_VIEW)
            ->addPermission(Permissions::ADMIN_NAVIGATION_VIEW)
            ->addPermission(Permissions::ADMIN_USERS_ADD)
            ->addPermission(Permissions::ADMIN_USERS_BAN)
            ->addPermission(Permissions::ADMIN_USERS_EDIT)
            ->addPermission(Permissions::ADMIN_USERS_REMOVE)
            ->addPermission(Permissions::ADMIN_USERS_SEND_MESSAGE)
            ->addPermission(Permissions::ADMIN_USERS_VIEW);

        return $adminRole;
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $rolesRepo = $this->getEntityManager()->getRepository(Role::class);
        $this->getEntityManager()->remove($rolesRepo->findOneBy(['name' => 'guest']));
        $this->getEntityManager()->flush();
    }
}
