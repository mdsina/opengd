<?php

namespace TestsUtils;


abstract class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Zend\Mvc\Application
     */
    private $application;

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    private $serviceManager;

    protected final function setUp()
    {
        parent::setUp();

        $this->serviceManager = \Bootstrap::getServiceManager();
        $this->application = \Bootstrap::getApplication();

        $this->onSetUp();
    }

    protected function onSetUp() {}

    /**
     * @return \Zend\Mvc\Application
     */
    protected function getApplication()
    {
        return $this->application;
    }

    /**
     * @return \Zend\ServiceManager\ServiceManager
     */
    protected function getServiceManager()
    {
        return $this->serviceManager;
    }
}