Ext.define('Ext.overrides.form.Basic', {
    override: 'Ext.form.Basic',

    /**
     * @private
     * Called after an action is performed via {@link #doAction}.
     * @param {Ext.form.action.Action} action The Action instance that was invoked
     * @param {Boolean} success True if the action completed successfully, false, otherwise.
     */
    afterAction: function (action, success) {
        var me = this;
        if (action.waitMsg) {
            var messageBox = Ext.MessageBox,
                waitMsgTarget = me.waitMsgTarget;
            if (waitMsgTarget === true) {
                me.owner.el.unmask();
            } else if (waitMsgTarget) {
                waitMsgTarget.unmask();
            } else {
                messageBox.hide();
            }
        }
        // Restore setting of any floating ancestor which was manipulated in beforeAction
        if (me.floatingAncestor) {
            me.floatingAncestor.preventFocusOnActivate = me.savePreventFocusOnActivate;
        }
        if (success) {
            if (action.reset) {
                me.reset();
            }
            Ext.callback(action.success, action.scope || action, [me, action]);
            me.fireEvent('actioncomplete', me, action);
        } else {
            if (action.failure) {
                Ext.callback(action.failure, action.scope || action, [me, action]);
            } else {
                me.handleFailure(action);
            }
            me.fireEvent('actionfailed', me, action);
        }
    },

    /**
     * @private
     * Form action error handler.
     * @param {Ext.form.action.Action} action
     */
    handleFailure: function (action) {
        var me = this;
        me.realOwner = me.owner;

        switch (action.failureType) {
            // CONNECT_FAILURE
            case Ext.form.action.Action.CONNECT_FAILURE:
                var response = Ext.JSON.decode(action.response.responseText, true),
                    message = (response && response.errorAlert)
                        ? response.errorAlert
                        : action.response.status + ': ' + action.response.statusText;
                if (response && response.hasOwnProperty('notLoggedAlert')) {
                    me.notLoggedAlert(response.notLoggedAlert);
                    break;
                }

                Ext.Msg.alert('Ошибка', message, function () {
                    if (me.realOwner.up && me.realOwner.up('.window')) {
                        me.realOwner.up('.window').close();

                        return;
                    }
                    me.realOwner.close();
                });
                break;

            // SERVER_INVALID
            case Ext.form.action.Action.SERVER_INVALID:
                if (
                    !action.result.errorAlert
                    && !action.result.errorValidate
                    && !action.result.errorCallback
                ) {
                    me.defaultError();

                    return;
                }
                me.notLoggedAlert(action.result.notLoggedAlert);
                me.errorAlert(action.result.errorAlert);
                me.errorValidate(action.result.errorValidate);
                me.errorCallback(action.result.errorCallback);
                break;

            // LOAD_FAILURE
            case Ext.form.action.Action.LOAD_FAILURE:
                if (!action.result.errorAlert && !action.result.errorCallback) {
                    me.defaultError();
                }

                me.errorAlert(action.result.errorAlert);
                me.errorCallback(action.result.errorCallback);
                break;
        }

        me.realOwner.setLoading(false);
    },

    /**
     * @private
     * Handle `errorAlert` errors.
     * @param {string|array}error
     */
    errorAlert: function (error) {
        if (!error) return;

        if (Ext.isString(error) || Ext.isArray(error)) {
            if (Ext.isString(error) && error == 'У вас недостаточно прав для данного действия!') {
                if (this.realOwner.up && this.realOwner.up('.window')) {
                    this.realOwner.up('.window').close();

                    return;
                }
                this.realOwner.close();
            }

            error = Ext.isString(error) ? error : error.join('<br />');

            Ext.Msg.alert('Ошибка', error);
        }
    },

    /**
     * @private
     * Handle `notLoggedAlert` errors.
     * @param {string|array}error
     */
    notLoggedAlert: function (error) {
        if (!error) return;

        if (Ext.isString(error) || Ext.isArray(error)) {
            if (Ext.isString(error) && error == 'У вас недостаточно прав для данного действия!') {
                if (this.realOwner.up && this.realOwner.up('.window')) {
                    this.realOwner.up('.window').close();

                    return;
                }
                this.realOwner.close();
            }

            error = Ext.isString(error) ? error : error.join('<br />');

            Ext.MessageBox.show({
                title: 'Ошибка',
                msg: error,
                buttonText: {
                    yes: "Войти в систему"
                },
                fn: function (btn) {
                    if (btn == 'yes') {
                        window.location.reload();
                    }
                }
            });
        }
    },

    /**
     * @private
     * Handle `errorValidate` errors.
     * @param {array} error
     */
    errorValidate: function (error) {
        if (!error) return;
        Ext.Msg.alert('Сообщение', 'Некоторые поля заполнены некорректно');
        this.markInvalid(error);
    },

    /**
     * @private
     * Handle `errorCallback`.
     * @param {string} callback Name a callback function
     */
    errorCallback: function (callback) {
        if (!callback) return;
        this.realOwner[callback]();
    },

    /**
     * @private
     * Handle `defaultError`.
     */
    defaultError: function () {
        var me = this;

        Ext.Msg.alert('Ошибка', 'Ошибка сервера!', function () {
            if (me.realOwner.up && me.realOwner.up('.window')) {
                me.realOwner.up('.window').close();


            }
        });
    }
});