Ext.define('Ext.overrides.form.field.VTypes', {
    override: 'Ext.form.field.VTypes',

    /**
     * @param value
     * @param field
     * @return {Boolean}
     */
    url: function(value, field) {
        return /^(https?:\/\/)?([0-9]{1,3}|([а-яё0-9a-z_!~*'()-]+\.)*([а-яё0-9a-z][а-яё0-9a-z-]{0,61})+[а-яё0-9a-z]\.[а-яёa-z]{2,6})?((\/?)|(\/[а-яё0-9a-z_!~*'().;?:@&=+$,%#-]+)+\/?)$/i.test(value);
    },

    urlText: '{0} указан неверно',

    /**
     * Проверка сущствования введенного значения в дропдаун-списке
     * компонента 'combobox'.
     *
     * @param value
     * @param field
     * @returns {Boolean}
     */
    exists: function(value, field) {
        if (field.getXType() != 'combobox') {
            return true;
        }

        var models = field.store.getRange(),
            displayField = field.displayField,
            exist;

        Ext.Array.forEach(models, function(model) {
            if (model.get(displayField) == value) {
                exist = true;
                return false;
            }
        });

        return exist;
    },

    existsText: 'Неверное значение в поле "{0}". Обратите внимание: необходимо выбирать значение из выпадающего списка.',

    /**
     * Только русские символы
     *
     * @param value
     * @param field
     * @return {Boolean}
     */
    alpharus: function(value, field) {
        return /[а-яА-Я]+/.test(value);
    },

    alpharusText: 'Только русские буквы!',

    /**
     * Только целочисленные
     *
     * @param value
     * @param field
     * @return {Boolean}
     */
    digit: function(value, field) {
        return /^\d+$/.test(value);
    },

    digitText: 'Поле "{0}" должно быть числовым!',

    emailMask: false,


    /**
     * Числа с точкой
     */
    floatnum: function(value, field) {
        return /(^\d+$|^\d+[.,]\d+$)/.test(value);
    },

    floatnumText: 'Поле "{0}" должно быть числом целым либо с дробной частью через , или .',

    /**
     * месяц.год
     */
    monthYear: function(value, field) {

        return /(0[1-9]|1[012])\.[0-9]{4}/.test(value);
    },

    monthYearText: 'Поле "{0}" должно быть датой в формате месяц.год (мм.гггг)',

    /**
     * месяц.год
     */
    yearTextField: function(value, field) {
        var from = field.from || 1900;
        var to = field.to || 3000;
        return /[0-9]{4}/.test(value) && (parseInt(value) >= from) && (parseInt(value) <= to) ;
    },

    yearTextFieldText: 'Поле "{0}" должно содержать год'
});
