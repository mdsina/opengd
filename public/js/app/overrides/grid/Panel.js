Ext.define('Ext.overrides.grid.Panel', {
    override: 'Ext.grid.Panel',
    emptyText: 'Нет записей!',
    viewConfig: {
        deferEmptyText: false,
        enableTextSelection: true
    },
    columnLines: true,
    rowLines: true
});
