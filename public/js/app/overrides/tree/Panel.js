Ext.define('Ext.overrides.tree.Panel', {
    override: 'Ext.tree.Panel',
    emptyText: 'Нет записей!',
    viewConfig: {
        deferEmptyText: false,
        enableTextSelection: true
    },
    columnLines: true,
    rowLines: true
});
