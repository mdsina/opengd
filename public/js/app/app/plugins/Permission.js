Ext.define('Gdru.plugins.Permission', {
    extend: 'Ext.AbstractPlugin',
    alias: 'plugin.permission',
    pluginId: 'Permission',
    permissionOptions: {},

    init: function (component) {
        var me = this;
        if (component.getXType() == 'treepanel') {
            var deniedNodes = [];
            component.getStore().getRootNode().cascadeBy(function (node) {
                var hasPermission = true;
                me.permissionOptions = {};
                if (node.raw.permission) {
                    me.initPermission(node.raw);
                    hasPermission = me.parsePermission(node.raw.permission)
                }

                if (!hasPermission) {
                    deniedNodes.push(node);
                }
            });
            Ext.each(deniedNodes, function (node) {
                node.remove();
            });
        } else {
            var children = component.query('*[permission]');

            if (component.permission) {
                children.push(component);
            }
            Ext.Array.each(children, function (cmp) {
                //debugger;
                var hasPermission = false;
                me.initPermission(cmp);
                if (cmp.permission.constructor === Array) {
                    Ext.Array.each(cmp.permission, function(rec) {
                        hasPermission = hasPermission || me.parsePermission(rec);
                    });
                } else if (cmp.permission) {
                    hasPermission = me.parsePermission(cmp.permission);
                }

                if (!cmp.permission) {
                    hasPermission = true;
                }

                if (!hasPermission) {
                    cmp.disabled = true;
                    var permissionStrategy = cmp.permissionStrategy || 'hidden';
                    cmp[permissionStrategy] = true;
                }

            });
        }
    },

    initPermission: function (obj) {
        var me = this;
        me.permissionOptions = {};

        if (obj.hasOwnProperty('extraPermission')) {
            if (typeof obj.extraPermission == 'function') {
                obj.extraPermission = obj.extraPermission();
            }
            me.permissionOptions['extraPermission'] = obj.extraPermission;
        }
    },

    getPermission: function (name) {
        var me = this;
        var data = Ext.apply(me.permissionOptions, {permission: name});
        return Gdru.utils.Auth.getGrantByPermission(data);
    },

    parsePermission: function (name) {
        var me = this;
        var regex = new RegExp('([a-zA-Z0-9._-]+)', 'g');
        var parsedString = name.replace(regex, 'me.getPermission(\'$&\')');
        return eval('(' + parsedString + ');');
    }
});
