Ext.define('Gdru.ux.DismissableAlert', {
    extend: 'Ext.Component',
    alias: 'widget.dismissalert',

    defaultMainCls: 'alert',
    defaultCls: 'alert-info',
    type: '',
    text: '',
    initComponent: function() {
        var me = this,
            type = me.defaultMainCls + ' ';

        if (me.type && me.type.trim() != '') {
            type += me.type;
        } else {
            type += me.defaultCls;
        }

        Ext.apply(me, {
            html: me.text,
            cls: type
        });

        me.callParent(arguments);
    }
});