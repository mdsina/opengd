Ext.define('Gdru.ux.TitleManager', {
    singleton: true,

    titleSeparator: '|',

    setTitle: function(title) {
        document.title = title;
    },

    updateTitle: function (subTitle) {
        document.title = Gdru.Settings.projectTitle + " " + this.titleSeparator + " " + subTitle;
    }
});