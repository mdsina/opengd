Ext.define('Gdru.ux.Grid.Column.Action', {
    extend: 'Ext.grid.column.Action',
    alias: 'widget.bigactioncolumn',
    alternateClassName: 'Gdru.grid.ActionColumn',

    innerCls: Ext.baseCSSPrefix + 'grid-cell-inner-action-col',
    defaultSeparator: '|',
    separator: '',

    defaultRenderer: function (v, cellValues, record, rowIdx, colIdx, store, view) {
        var me = this,
            prefix = Ext.baseCSSPrefix,
            scope = me.origScope || me,
            items = me.items,
            len = items.length,
            i = 0,
            item, ret, link = ' ', links = [], disabled, tooltip;

        ret = Ext.isFunction(me.origRenderer)
            ? me.origRenderer.apply(scope, arguments) || ''
            : '';

        cellValues.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';

        for (; i < len; i++) {
            item = items[i];

            disabled = item.disabled || (
                item.isDisabled
                    ? item.isDisabled.call(item.scope || scope, view, rowIdx, colIdx, item, record)
                    : false
            );

            tooltip = disabled ? null : (
                item.tooltip || (
                    item.getTip
                        ? item.getTip.apply(item.scope || scope, arguments)
                        : null
                )
            );

            var isVisible = typeof item.isVisible == 'function'
                ? item.isVisible(record)
                : item.isVisible || true;

            if (!isVisible) {
                continue;
            }

            if (!item.hasActionConfiguration) {
                item.stopSelection = me.stopSelection;
                item.disable = Ext.Function.bind(me.disableAction, me, [i], 0);
                item.enable = Ext.Function.bind(me.enableAction, me, [i], 0);
                item.hasActionConfiguration = true;
            }

            link = '<a role="button" href="javascript:void(0);" style="margin: 0 4px; " title="' + (item.title || me.title) + '"' +
                '  class="' + prefix + 'action-text ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
                ' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : '') + '"' +
                (tooltip ? ' data-qtip="' + tooltip + '"' : '') + ' >';

            if (!item.hasOwnProperty('iconPosition') || item.iconPosition == null) {
                item.iconPosition = 's';
            }

            var vText = (item.text || me.text),
                iconCls = item.iconCls || me.iconCls || '',
                vImg = '';

            // @TODO: Добавить условие, что если класс иконки не задан, но есть ссылка на картинку иконки то вставить как <img />
            if (iconCls != '') {
                vImg = '<div style="margin-right: 2px" class="' + me.actionIconCls + ' ' +
                    (item.iconCls || me.iconCls || '') + '"' +
                    (tooltip ? ' data-qtip="' + tooltip + '"' : '') + '></div>';
            }

            // "s" - start, "e" или что-то еще - end
            if (item.iconPosition == 's') {
                link += vImg + vText;
            } else {
                link += vText + vImg;
            }

            link += '</a>';

            links.push(link);
        }

        return ret + links.join((me.hasOwnProperty('separator') && me.separator != null && me.separator != '' ? me.separator : me.defaultSeparator));
    }
});

//Ext.define('Gdru.ux.Grid.Column.Action', {
//    alias: 'widget.bigactioncolumn',
//    extend: 'Ext.grid.column.Action',
//    text: 'Действия',
//    innerCls: Ext.baseCSSPrefix + 'grid-cell-inner-text-action-col',
//
//    defaultRenderer: function (v, meta, record, rowIdx, colIdx, store, view) {
//        var me = this,
//            prefix = Ext.baseCSSPrefix,
//            scope = me.origScope || me,
//            items = me.items,
//            len = items.length,
//            i = 0,
//            item, ret, link = ' ', links = [], disabled, tooltip;
//
//        ret = Ext.isFunction(me.origRenderer) ? me.origRenderer.apply(scope, arguments) || ' ' : ' ';
//        meta.tdCls += ' ' + Ext.baseCSSPrefix + 'text-action-col-cell';
//        for (; i < len; i++) {
//            item = items[i];
//
//            disabled = item.disabled || (item.isDisabled ? item.isDisabled.call(item.scope || scope, view, rowIdx, colIdx, item, record) : false);
//            tooltip = disabled ? null : (item.tooltip || (item.getTip ? item.getTip.apply(item.scope || scope, arguments) : null));
//
//            var isVisible = typeof item.isVisible == 'function' ? item.isVisible(record) : item.isVisible || true;
//
//            if (!isVisible) {
//                continue;
//            }
//
//            // Only process the item action setup once.
//            if (!item.hasActionConfiguration) {
//
//                // Apply our documented default to all items
//                item.stopSelection = me.stopSelection;
//                item.disable = Ext.Function.bind(me.disableAction, me, [i], 0);
//                item.enable = Ext.Function.bind(me.enableAction, me, [i], 0);
//                item.hasActionConfiguration = true;
//            }
//
//
//            link = '<a href="javascript:void(0);" style="margin: 0 4px;" title="' + (item.title || me.title) + '"  class="' + prefix + 'action-text ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
//                ' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : (item.iconCls || me.iconCls || '')) + '"' +
//                (tooltip ? ' data-qtip="' + tooltip + '"' : '') + ' >' + (item.text || me.text) + '</a>';
//            links.push(link);
//        }
//        return ret + links.join('|');
//    },
//    constructor: function () {
//        this.callParent(arguments);
//        Ext.each(this.items, function () {
//            var handler;
//            if (this.action) {
//                handler = this.handler;
//                this.handler = function (view, rowIdx, colIdx, item, e, record) {
//                    var grid = view.up('grid');
//                    grid.fireEvent(item.action, record, item, grid, rowIdx, colIdx);
//                    handler && handler.apply(this, arguments);
//                };
//            }
//        });
//    },
//
//    initComponent: function () {
//        var me = this;
//
//        if (me.hasOwnProperty('multiline') && me.multiline) {
//            me.text += '<br/> &nbsp;';
//        }
//
//        var resetFilter = me.initialConfig.resetFilter;
//        if (resetFilter) {
//            me.filterElement = Ext.create('Ext.button.Button', {text: 'Сбросить фильтр'});
//        }
//
//        me.callParent(arguments);
//    }
//});