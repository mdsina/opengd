Ext.define('Gdru.Application', {
    name: 'Gdru',
    extend: 'Ext.app.Application',

    requires: [
        "Ext.*",
        "Ext.grid.*",
        "Ext.form.*",
        "Ext.toolbar.*",
        "Ext.button.*",
        'Gdru.*',
        'Gdru.ux.TitleManager'
    ],

    controllers: [
        'Base',
        'Forum',
        'Users',
        'Administrate',
        'Access'
    ],

    stores: [
        'Gdru.store.Navigation'
    ],

    launch: function() {
        Gdru.ux.TitleManager.setTitle(Gdru.Settings.projectTitle);
        Ext.GlobalEvents.fireEvent('beforelaunch');
    }
});
