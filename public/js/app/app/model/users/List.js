Ext.define('Gdru.model.users.List', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'username', type: 'string'},
        {name: 'email', type: 'string'},
        {name: 'firstName', type: 'string'},
        {name: 'secondName', type: 'string'}
    ]
});