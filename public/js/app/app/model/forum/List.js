Ext.define('Gdru.model.forum.List', {
    extend: 'Ext.data.TreeModel',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'title', type: 'string'},
        {name: 'manuallyCreated', type: 'boolean'},
        {name: 'parent', type: 'string'},
        {name: 'name', type: 'string'},
        {name: 'disabled', type: 'boolean'}
    ]
});