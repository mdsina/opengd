Ext.define('Gdru.store.roles.List', {
    extend: 'Ext.data.Store',
    model: 'Gdru.model.roles.List',
    remoteFilter: true,
    remoteSort: true,
    proxy: {
        url: '/admin-backend/role/list',
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});