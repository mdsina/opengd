Ext.define('Gdru.store.Navigation', {
    extend: 'Ext.data.TreeStore',
    root: {
        expanded: true,
        "children": []
    },
    proxy: {
        url: '/admin-backend/main/get-navigation',
        type: 'ajax',
        reader: {
            type: 'json'
        }
    },
    autoLoad: false
});
