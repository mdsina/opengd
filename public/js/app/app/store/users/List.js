Ext.define('Gdru.store.users.List', {
    extend: 'Ext.data.Store',
    model: 'Gdru.model.users.List',
    remoteFilter: true,
    remoteSort: true,
    proxy: {
        url: '/admin-backend/user/list',
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});