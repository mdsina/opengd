Ext.define('Gdru.store.forum.List', {
    extend: 'Ext.data.TreeStore',
    model: 'Gdru.model.forum.List',
    proxy: {
        url: '/admin-backend/forum/list',
        type: 'ajax'
    },
    folderSort: true,
    rootVisible: false,
    autoLoad: false,
    root:{
        text: '',
        expanded:true,
        children:[]
    }
});