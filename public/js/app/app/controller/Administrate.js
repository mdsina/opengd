Ext.define('Gdru.controller.Administrate', {
    extend: 'Gdru.controller.Base',

    views: [
        'administration.roles.List'
    ],

    routes: {
        'administrate/roles': 'onRoles',
        'administrate/permissions': 'onPermissions'
    },

    stores: [
        'roles.List'
    ],

    refs: [
        {ref: 'RolesList', xtype: 'rolesList', autoCreate: true}
    ],

    onRoles: function () {
        this.renderToWorkSpace(this.getRolesList(), false);
    },

    onPermissions: function () {
        console.log('OnPermis');
    }
});
