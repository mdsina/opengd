Ext.define('Gdru.controller.Base', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.History'
    ],

    renderToWorkSpace: function (component, replaceWorkspace) {
        var workSpace;

        workSpace = Ext.ComponentQuery.query('[xtype=viewer]')[0];

        this.renderToTabPanel(workSpace, component, replaceWorkspace);
    },

    renderToDashboard: function (component) {
        if (!component) {
            return;
        }

        var dashboard = Ext.ComponentQuery.query('dashboard2')[0];

        this.renderToTabPanel(dashboard, component, false);
    },

    renderToTabPanel: function (source, component, replace) {
        if (replace == null || replace == 'undefined') {
            var replace = true;
        }

        var tab = source.getComponent(component.getId());

        if (replace) {
            source.removeAll(true);
        }

        if (!tab) {
            Ext.applyIf(component, {
                token: Ext.History.getToken()
            });

            tab = source.add(component);
        }

        Gdru.ux.TitleManager.updateTitle(tab.title);

        tab.token = Ext.History.getToken();
        source.setActiveTab(tab);
    }
});