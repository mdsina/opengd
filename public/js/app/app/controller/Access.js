Ext.define('Gdru.controller.Access', {
    extend: 'Gdru.controller.Base',

    init: function () {
        var me = this;

        me.listen({
            controller: {},
            component: {},
            global: {
                'beforelaunch': me.loadPermissions,
                'permissionsloadfail': me.onFailedPermissionsLoading,
                'permissionsload': me.onPermissionsLoad,
                'userpermissionsload': me.onUserPermissionsLoad
            },
            store: {}
        });
    },

    loadPermissions: function () {
        Gdru.utils.Permissions.init();

    },

    onFailedPermissionsLoading: function () {
        console.log('failed');
    },

    onPermissionsLoad: function () {
        Gdru.utils.UserPermissions.init();
    },

    onUserPermissionsLoad: function () {
        Ext.create('Gdru.view.Viewport');
    }
});
