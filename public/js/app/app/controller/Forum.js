Ext.define('Gdru.controller.Forum', {
    extend: 'Gdru.controller.Base',

    views: [
        'forum.List',
        'forum.Form',
        'forum.add.Window',
        'forum.edit.Window'
    ],

    routes: {
        'forums': 'onForums'
    },

    stores: [
        'forum.List'
    ],

    refs: [
        {ref: 'ForumList', xtype: 'forumsList', autoCreate: true}
    ],

    onForums: function () {
        this.renderToWorkSpace(this.getForumList(), false);
    }
});
