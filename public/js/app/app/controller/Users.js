Ext.define('Gdru.controller.Users', {
    extend: 'Gdru.controller.Base',

    views: [
        'users.List'
    ],

    routes: {
        'users': 'onUsers'
    },

    stores: [
        'users.List'
    ],

    refs: [
        {ref: 'UserList', xtype: 'usersList', autoCreate: true}
    ],

    onUsers: function () {
        this.renderToWorkSpace(this.getUserList(), false);
    }
});
