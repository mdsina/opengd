Ext.define('Gdru.utils.UserPermissions', {
    singleton: true,
    init: function () {
        Ext.Ajax.request({
            url: '/auth/get-current-permissions/simple',
            async: false,
            success: function (r) {
                var response = Ext.JSON.decode(r.responseText, true);

                Ext.apply(Gdru.utils.UserPermissions, response.data);
                Ext.GlobalEvents.fireEvent('userpermissionsload')
            },
            failure: function (response, opts) {
                Ext.GlobalEvents.fireEvent('userpermissionsloadfail');
            }
        });
    }
});
