Ext.define('Gdru.utils.Auth', {
    singleton: true,
    rootUrl: '/auth/',

    getGrantByPermission: function (data) {
        var me = this;
        return (data.hasOwnProperty('extraPermission') ? data.extraPermission : true) & me.hasAccess(data);
    },

    hasAccess: function (data) {
        var me = this,
            hasPermission = false;

        if (data.permission.constructor === Array) {
            Ext.Array.each(data.permission, function(rec) {
                hasPermission = hasPermission || me.hasAccess({permission: rec});
            });

            return hasPermission;
        }

        if (data.permission && Gdru.utils.UserPermissions.data[data.permission]) {
            return true;
        }

        return false;
    },

    isGranted: function (controller, action, cb) {
        var me = this,
            errorMessage = new Ext.window.MessageBox({
                title: 'Ошибка',
                msg: 'Не удалось проверить разрешенность действия'
            });

        Ext.Ajax.request({
            url: me.rootUrl + 'is-granted',
            async: false,
            params: {
                controller: controller,
                action: action
            },
            success: function (response) {
                var answer = Ext.decode(response.responseText);

                if (answer.granted) {
                    cd();
                }
            },
            failure: function () {
                errorMessage.show();
            }
        });
    }
});
