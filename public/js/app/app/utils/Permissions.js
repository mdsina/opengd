Ext.define('Gdru.utils.Permissions', {
    singleton: true,
    init: function () {
        Ext.Ajax.request({
            url: '/auth/get-permissions-list',
            async: false,
            success: function (r) {
                var response = Ext.JSON.decode(r.responseText, true);

                Ext.apply(Gdru.utils.Permissions, response.data);
                Ext.GlobalEvents.fireEvent('permissionsload')
            },
            failure: function (response, opts) {
                Ext.GlobalEvents.fireEvent('permissionsloadfail');
            }
        });
    }
});
