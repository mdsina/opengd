Ext.define('Gdru.view.forum.Form', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldContainer'
    ],
    alias: 'widget.forumForm',

    initComponent: function () {
        var me = this,
            loadItem = false;

        Ext.apply(me, {
            bodyPadding: 10,
            defaultType: 'textfield',
            fieldDefaults: {
                anchor: '100%',
                msgTarget: 'side',
                blankText: 'Поле не должно быть пустым!',
                maxLength: 255,
                allowBlank: false
            },
            items: [
                {
                    fieldLabel: 'Имя',
                    name: 'fsOne[name]'
                },
                {
                    fieldLabel: 'Заголовок',
                    name: 'fsOne[title]'
                },
                {
                    fieldLabel: 'Описание',
                    name: 'fsOne[description]'
                }
            ],
            buttons: [
                {
                    text: 'Сохранить',
                    handler: function () {
                        // TODO: Вынести в шаблонный класс и вообще во ViewController
                        var form = me.getForm();
                        if (form.isValid()) {
                            form.submit({
                                waitMsg: "Сохранение..",
                                success: function (form, action) {
                                    me.up('.window').fireEvent('forumSaved');

                                    Ext.Msg.show({
                                        title: 'Успешно',
                                        message: action.result.msg,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.INFO,
                                        fn: function(btn) {
                                            if (btn === 'ok') {
                                                me.up('.window').close();
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            Ext.Msg.alert('Ошибка', 'Поля формы заполнены некорректно.')
                        }
                    }
                }
            ]
        });

        if (me.hasOwnProperty('params') && me.params) {
            var currentItem = me.params.hasOwnProperty('data') ? me.params.data : false;

            if (me.params.hasOwnProperty('loadItem')) {
                loadItem = me.params.loadItem;
            }

            if (currentItem) {
                Ext.apply(me, {
                    dockedItems: [{
                        xtype: 'toolbar',
                        dock: 'top',
                        layout: 'fit',
                        style: {
                            background: '#f3f3f3',
                            paddingTop: 10
                        },
                        items: [{
                            xtype: 'dismissalert',
                            anchor: '100%',
                            text: 'Выбранный форум: #' + currentItem.get('id')
                        }]
                    }]
                });

                me.items.push({
                    xtype: 'hiddenfield',
                    name: 'fsOne[parent]',
                    value: currentItem.get('id')
                });
            }
        }

        me.callParent(arguments);

        if (loadItem) {
            var form = me.getForm();

            // TODO: Гидратор не нужен, нужно подумать над нормальным биндингом. Проблема с названием полей в модели и в форме, поэтому не получится сделать form.loadRecord(currentItem.data)
            form.findField('fsOne[name]').setValue(currentItem.get('name'));
            form.findField('fsOne[title]').setValue(currentItem.get('title'));
            form.findField('fsOne[description]').setValue(currentItem.get('description'));
            form.findField('fsOne[parent]').setValue(currentItem.get('parent'));
        }
    }
});
