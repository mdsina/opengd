Ext.define('Gdru.view.forum.edit.Window', {
    extend: 'Ext.window.Window',
    autoShow: true,
    modal: true,
    alias: 'widget.forumEditWin',
    title: 'Редактирование форума',
    resizable: true,
    layout: 'fit',
    resizeHandles: 'w e',
    width: 500,

    initComponent: function () {
        var me = this,
            form = Ext.create('Gdru.view.forum.Form', {
            url: '/admin-backend/forum/edit',
            params: me.hasOwnProperty('params') ? me.params : {}
        });

        Ext.apply(this, {
            items: [
                form
            ]
        });

        this.callParent(arguments);
    }
});
