Ext.define('Gdru.view.forum.List', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.forumsList',
    id: 'forumsListId',
    title: 'Список форумов',
    scroll: true,
    fixed: true,
    closable: true,
    controller: 'forums',
    plugins: ['permission'],
    //viewConfig: {
    //    plugins: {
    //        ptype: 'treeviewdragdrop'
    //    },
    //    listeners: {
    //        beforedrop: function (node, data) {
    //            console.log([node, data]);
    //        },
    //        drop: function (node, data, dropRec, dropPosition) {
    //            console.log([node, data, dropRec, dropPosition]);
    //        }
    //    }
    //},
    requires: [
        'Ext.menu.Menu'
    ],

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    style: {
                        background: '#f3f3f3',
                        paddingTop: 10
                    },
                    items: [
                        {
                            text: 'Обновить',
                            iconCls: 'x-fa fa-refresh',
                            handler: 'onUpdate'
                        },
                        {
                            xtype: 'splitbutton',
                            text: 'Добавить',
                            permission: Gdru.utils.Permissions.ADMIN_FORUMS_ADD,
                            iconCls: 'x-fa fa-plus-square',
                            handler: 'onButtonAdd',
                            menu: new Ext.menu.Menu({
                                items: [
                                    {
                                        text: 'Добавить корень',
                                        handler: 'onAddRoot'
                                    },
                                    {
                                        text: 'Добавить к выбранному',
                                        itemId: 'addToChoosed',
                                        handler: 'onAddChild'
                                    }
                                ]
                            }),
                            listeners: {
                                menushow: 'onMenuShow'
                            }
                        }
                    ]
                }
            ],
            store: 'forum.List',
            collapsible: true,
            useArrows: true,
            rootVisible: false,
            multiSelect: false,
            columns: {
                defaults: {
                    menuDisabled: true,
                    resizable: true,
                    align: 'left',
                    flex: 1
                },
                items: [
                    {
                        header: '#ID',
                        dataIndex: 'id',
                        maxWidth: 50
                    },
                    {
                        xtype: 'treecolumn',
                        text: 'Заголовок',
                        sortable: true,
                        dataIndex: 'title'
                    },
                    {
                        xtype: 'actioncolumn',
                        items: [
                            {
                                iconCls: 'x-fa fa-pencil-square-o text-orange',
                                tooltip: 'Редактировать',
                                handler: 'onForumEdit'
                            },
                            {
                                iconCls: 'x-fa fa-trash-o text-red',
                                tooltip: 'Удалить',
                                handler: 'onForumRemove'
                            }
                        ]
                    }
                ]
            }
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'onUpdate',
        scope: 'controller'
    }
});
