Ext.define('Gdru.view.forum.add.Window', {
    extend: 'Ext.window.Window',
    autoShow: true,
    modal: true,
    alias: 'widget.forumAddWin',
    title: 'Добавление форума',
    resizable: true,
    layout: 'fit',
    resizeHandles: 'w e',
    width: 500,

    initComponent: function () {
        var me = this,
            form = Ext.create('Gdru.view.forum.Form', {
            url: '/admin-backend/forum/create',
            params: me.hasOwnProperty('params') ? me.params : {}
        });

        Ext.apply(this, {
            items: [
                form
            ]
        });

        this.callParent(arguments);
    }
});
