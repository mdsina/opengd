Ext.define('Gdru.view.forum.ForumsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.forums',

    onUpdate: function () {
        var me = this.getView();
        me.getStore().load();
    },

    onAdd: function (data) {
        var win = Ext.widget('forumAddWin', data),
            me = this.getView();

        win.on('forumSaved', function () {
            me.getStore().load();
        });
    },

    onAddChild: function () {
        var me = this.getView();
        this.onAdd({
            params: {data: me.getSelectionModel().getSelection()[0]}
        });
    },

    onAddRoot: function () {
        this.onAdd();
    },

    onMenuShow: function (that, menu, eOpts) {
        var me = this.getView();

        if (!me.getSelectionModel().hasSelection()) {
            that.menu.items.get('addToChoosed').setDisabled(true);
        } else {
            that.menu.items.get('addToChoosed').setDisabled(false);
        }
    },

    onButtonAdd: function () {
        var me = this.getView();

        if (me.getSelectionModel().hasSelection()) {
            this.onAddChild();
        } else {
            this.onAddRoot();
        }
    },

    onForumEdit: function (grid, rowIndex, colIndex) {
        var me = this.getView(), win;
        var rec = grid.getStore().getAt(rowIndex);

        win = Ext.widget('forumEditWin', {
            params: {
                data: rec,
                loadItem: true
            }
        });

        win.on('forumSaved', function () {
            me.getStore().load();
        });
    },

    onForumRemove: function (grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex),
            me = this.getView();
        Ext.Msg.show({
            title: 'Удалить запись?',
            message: 'Вы действительно хотите удалить запись #' + rec.get('id') +
            '? <br>Все дочерние записи будут удалены.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                if (btn === 'yes') {
                    me.setLoading(true);
                    Ext.Ajax.request({
                        url: '/admin-backend/forum/remove',
                        method: 'POST',
                        params: {
                            id: rec.get('id')
                        },
                        success: function (response) {
                            var text = Ext.JSON.decode(response.responseText);
                            me.setLoading(false);
                            if (text.success) {
                                grid.getStore().load();
                            }

                        },
                        failure: function (form, action) {
                            me.setLoading(false);
                        }
                    });
                }
            }
        });
    }
});