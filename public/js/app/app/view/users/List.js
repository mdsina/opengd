Ext.define('Gdru.view.users.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.usersList',
    id: 'usersListId',
    title: 'Список пользователей',
    scroll: true,
    fixed: true,
    closable: true,
    controller: 'users',

    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            store: 'users.List',
            ui: 'footer',
            displayInfo: true
        },
        {
            xtype: 'toolbar',
            dock: 'top',
            style: {
                background: '#f3f3f3',
                paddingTop: 10
            },
            items: [
                {
                    text: 'Обновить',
                    itemId: 'updateBtn',
                    iconCls: 'x-fa fa-refresh',
                    handler: 'onUpdate',
                    scope: 'controller'
                },
                {
                    text: 'Добавить',
                    itemId: 'addBtn',
                    iconCls: 'x-fa fa-plus-square',
                    handler: 'onButtonAdd'
                }
            ]
        }
    ],
    store: 'users.List',
    collapsible: true,
    useArrows: true,
    rootVisible: false,
    multiSelect: false,
    columns: {
        defaults: {
            menuDisabled: true,
            resizable: true,
            align: 'left',
            flex: 1
        },
        items: [
            {
                header: '#ID',
                dataIndex: 'id',
                maxWidth: 50
            },
            {
                header: 'Имя пользователя',
                dataIndex: 'username'
            },
            {
                header: 'Email',
                dataIndex: 'email'
            },
            {
                menuDisabled: true,
                xtype: 'bigactioncolumn',
                items: [
                    {
                        iconCls: 'x-fa fa-pencil-square-o text-orange',
                        text: 'Редактировать',
                        tooltip: 'Редактировать',
                        handler: 'onUserEdit'
                    },
                    {
                        iconCls: 'x-fa fa-trash-o text-red',
                        tooltip: 'Удалить',
                        text: 'Удалить',
                        handler: 'onUserRemove'
                    }
                ]
            }
        ]
    },

    listeners: {
        afterrender: 'onUpdate',
        scope: 'controller'
    }
});
