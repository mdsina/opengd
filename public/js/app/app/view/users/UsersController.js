Ext.define('Gdru.view.users.UsersController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.users',

    onUpdate: function () {
        var me = this.getView();
        me.getStore().load();
    },

    onAdd: function () {

    },

    onButtonAdd: function () {
        console.log('asgasg');
    },

    onUserEdit: function (grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex);
        alert("Edit " + rec.get('firstname'));
    },

    onUserRemove: function (grid, rowIndex, colIndex) {
        var me = this.getView();
        var rec = grid.getStore().getAt(rowIndex);
        Ext.Msg.show({
            title: 'Удалить запись?',
            message: 'Вы действительно хотите удалить запись #' + rec.get('id'),
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                if (btn === 'yes') {
                    me.setLoading(true);
                    Ext.Ajax.request({
                        url: '/admin-backend/user/remove',
                        method: 'POST',
                        params: {
                            id: rec.get('id')
                        },
                        success: function (response) {
                            var text = Ext.JSON.decode(response.responseText);
                            me.setLoading(false);
                            if (text.success) {
                                grid.getStore().load();
                            }

                        },
                        failure: function (form, action) {
                            me.setLoading(false);
                        }
                    });
                }
            }
        });
    }
});