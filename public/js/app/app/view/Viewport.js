Ext.define('Gdru.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.layout.container.Border',
        'Gdru.view.dashboard.Dashboard'
    ],
    layout: 'border',
    minWidth: 1000,
    items: [
        {
            region: 'center',
            xtype: 'dashboard2'
        }
    ],

    listeners: {
        afterrender: function () {
            (new Ext.util.DelayedTask(function () {
                Ext.getBody().addCls('loaded');

                (new Ext.util.DelayedTask(function () {
                    Ext.get('loader-wrapper').destroy();
                })).delay(300);
            })).delay(2000);
        }
    }

});