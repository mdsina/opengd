Ext.define('Gdru.view.administration.roles.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.rolesList',
    id: 'rolesListId',
    title: 'Список ролей',
    scroll: true,
    fixed: true,
    closable: true,
    controller: 'roles',

    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            pageSize: 50,
            dock: 'bottom',
            store: 'roles.List',
            ui: 'footer',
            displayInfo: true
        },
        {
            xtype: 'toolbar',
            dock: 'top',
            style: {
                background: '#f3f3f3',
                paddingTop: 10
            },
            items: [
                {
                    text: 'Обновить',
                    iconCls: 'x-fa fa-refresh',
                    handler: 'onUpdate'
                },
                {
                    text: 'Добавить',
                    iconCls: 'x-fa fa-plus-square',
                    handler: 'onAdd'
                }
            ]
        }
    ],
    store: 'roles.List',
    collapsible: true,
    useArrows: true,
    rootVisible: false,
    multiSelect: false,
    columns: {
        defaults: {
            menuDisabled: true,
            resizable: true,
            align: 'left',
            flex: 1
        },
        items: [
            {
                header: '#ID',
                dataIndex: 'id',
                maxWidth: 50
            },
            {
                header: 'Название',
                dataIndex: 'name'
            },
            {
                header: 'Тип роли',
                dataIndex: 'type'
            },
            {
                menuDisabled: true,
                xtype: 'actioncolumn',
                items: [
                    {
                        iconCls: 'x-fa fa-pencil-square-o text-orange',
                        tooltip: 'Редактировать',
                        handler: 'onEdit'
                    },
                    {
                        iconCls: 'x-fa fa-trash-o text-red',
                        tooltip: 'Удалить',
                        handler: 'onRemove'
                    }
                ]
            }
        ]
    },
    listeners: {
        beforerender: 'onUpdate'
    }
});
