Ext.define('Gdru.view.dashboard.Dashboard', {
    extend: 'Ext.tab.Panel',

    requires: [
        'Ext.layout.container.Border',
        'Gdru.view.dashboard.Navigation',
        'Gdru.view.dashboard.ContentPanel'
    ],

    alias: 'widget.dashboard2',
    title: 'Панель управления сайтом',
    header: true,

    listeners: {
        beforetabchange: function (tabpanel, newCard) {
            Ext.util.History.add(newCard.token);
        }
    },
    items: [{
        title: 'Рабочая область',
        layout: 'border',
        id: 'cabinet',
        token: '/',
        items: [
            {
                region: 'west',
                xtype: 'dashboard2.navigation',
                width: 310
            },
            {
                region: 'center',
                xtype: 'viewer',
                style: 'border-left: 1px solid #ccc',
                listeners: {
                    tabchange: function (tabPanel, newCard, oldCard, eOpts) {
                        window.location.hash = '#' + newCard.token;
                    }
                }
            }
        ]
    }]
});