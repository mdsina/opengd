Ext.define('Gdru.view.dashboard.Navigation', {
    extend: 'Ext.tree.Panel',
    alias: [ 'widget.dashboard2.navigation', 'widget.navigation2' ],
    rootVisible: false,
    titleCollapse: true,
    id: 'dashboardNavigationId',
    scroll: 'vertical',
    closable: false,
    header: false,
    height: 200,
    width: 200,
    title: 'Навигационное меню',

    initComponent: function() {
        var me = this,
            store = Ext.create('Gdru.store.Navigation');

        Ext.apply(me, {
            store: store,
            tbar: {
                items: [
                    '->' , {
                        text: 'Скрыть',
                        iconAlign: 'right',
                        id: 'dashboardnavigationcloser',
                        handler: function() {
                            me.collapse();
                    }
                }]
            },
            listeners: {
                afterrender: function() {
                    me.getStore().load();
                },
                itemclick: function (object, node) {
                    if (!node || !node.raw.itemId || !node.raw.leaf) {
                        return;
                    }

                    var params = Ext.Object.merge({id: me.params.id}, node.raw.params || {}),
                        nodeId = node.raw.itemId + '_' + JSON.stringify(params),
                        xtype = node.raw.itemId.replace(/-/g, ''),
                        workspace = me.child('#workspace'),
                        tab = workspace.getComponent(nodeId) || workspace.add({
                            xtype: xtype,
                            itemId: nodeId,
                            params: Ext.Object.merge(me.params, node.raw.params || {})
                        });

                    workspace.setActiveTab(tab);
                }
            }
        });

        this.callParent(arguments);

    }
});