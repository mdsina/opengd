OpenGD - gdru clone
=======================

Project task tracker https://gamedevru.myjetbrains.com/youtrack/issues

Installation
------------
The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install dependencies command:

    curl -s https://getcomposer.org/installer | php --
    php composer.phar install

In file config/autoload/postgres.global.php you cand find some useful configuration for db connection. If you prefer other db, please, change driver for connection.

After that run in dirrectory of checkouted project
    
    vendor/bin/doctrine-module orm:schema-tool:update --dump-sql

If all is ok run

    vendor/bin/doctrine-module orm:schema-tool:update --force

It create tables in database

Then install Sencha CMD  https://www.sencha.com/products/extjs/cmd-download/

In directory public/js/app run

    sencha app build production

It's minify admin to one file